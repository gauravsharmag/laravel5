<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laravel\User;
use Laravel\Notifications\Demo;
use Laravel\Custom\Notification as NOTE;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::loginUsingId(7);
// Route::get('/',function() {
// 	Auth::user()->notify(new Demo);
// });

// Route::get('/',function(){
// 	$not=new NOTE();
// 	$var=$not->notifyAdmin([
// 		'user'=>8,
// 		'type'=>5,
// 		'msg'=>'New Test Message Again goes here...'
// 	]);  
// 	if($var) {
// 		return "Notification Sent";
// 	}
// 	return 'Sorry Notification Could not be sent';
// }); 

\DB::listen(function($sql) {
   // var_dump($sql);
});

Route::get('show_php',function() {
	phpinfo();
});


Route::view('/password/reset','users.password_reset')->name('reset_password'); 

Route::view('/','home.home')->name('home');
Route::get('/mail',function() {
	$user= Laravel\User::find(7); 
	if($user->role_id==1) {
		$user->role_id='Admin';
	}
	return new Laravel\Mail\NewUser($user); 
});
Route::get('/sendMail',function() {
	$user= User::find(7);
	if($user->role_id==1) {
		$user->role_id='Admin'; 
	}
	// Mail::to('gaurav@netgen.in')->send(new Laravel\Mail\NewUser($user));
	Mail::to('gaurav@netgen.in')->send(new Laravel\Mail\NewUser($user)); 
	// Mail::to('gaurav@netgen.in')->queue(new Laravel\Mail\NewUser($user));  
});  
Route::get('/lang/{lng}',function($lng) {
	App::setLocale($lng);
	session(['my_locale' => $lng]);
	return redirect()->back(); 
}); 

// Route::get('/all_users',function() {
// 	return Laravel\User::paginate(5);
// });
Route::get('/all_users','UsersController@pagination_demo');
Route::get('demo',function() {
	dd(public_path());
});

// Route::get('/', function () {   return view('home.home');  }); 

Route::get('/login',['as'=>'login','uses'=>'UsersController@signin' ]); 
Route::get('/signin','UsersController@signin');  
Route::post('/signin','UsersController@login'); 
Route::get('/signup','UsersController@signup'); 
Route::post('/signup','UsersController@saveUser');
Route::get('/logout','UsersController@logout');
Route::view('/about','about.about')->name('about');
// Route::get('/about',function() { return view('about.about'); }); 
Route::get('/contact','TicketsController@contact'); 
Route::post('/contact','TicketsController@store');   

Route::get('/trigger_hook','TicketsController@triggerHook');	
Route::post('/trigger_hook','TicketsController@triggerHook');	   

// Admin & User Routes i.e. Both Admin & User are allowed to access these routes
Route::group(['middleware'=>['auth','admin_customer']],function() {
	Route::get('/users/change_password','UsersController@showChangePassword');   
	Route::post('/users/change_password','UsersController@changePassword');
	Route::get('/posts/deleteImages/{id}','PostController@deleteImages');
	Route::get('/users/switch_back/{id}','UsersController@switchAccountBack');    
});

// User Routes i.e. Only authenticated user is allowed to access these routes
Route::group(['middleware' => ['auth','customer']], function () { 

	// Datatable routes
	Route::get('/posts/get_user_posts',['as'=>'datatable.getuserpost','uses'=>'PostController@getUserPosts']);
 
	Route::get('/users/user_dashboard',function() { return view('users.user_dashboard'); }); 
	Route::get('/users/user/{id}','UsersController@user');  
	Route::post('/users/user/{id}','UsersController@updateUserProfile'); 

	Route::get('/posts/user_posts','PostController@showUserPost'); 
	Route::get('/posts/add_user_post','PostController@showAddUserPost'); 
	Route::post('/posts/add_user_post','PostController@saveUserPost'); 

	Route::get('/posts/user_view/{id}','PostController@view'); 
	Route::get('/posts/user_edit/{id}','PostController@showUserEdit');
	Route::post('/posts/user_edit/{id}','PostController@updateUserPost');
	Route::get('/posts/delete_user_post/{id}','PostController@deleteUserPost');        
});     
  
// Admin Routes i.e. only Admin is allowed to access these routes
Route::group(['middleware'=>['auth','admin']],function() {      

	// Datatable routes 
	Route::get('/tickets/gettickets',['as'=>'datatable.gettickets','uses'=>'TicketsController@gettickets']);
	Route::get('/users/getUsers',['as'=>'datatable.getUsers','uses'=>'UsersController@getUsers']);
	Route::get('/posts/getposts',['as'=>'datatable.getposts','uses'=>'PostController@getPosts']);   
	Route::get('/posts/gettwitter',['as'=>'datatable.gettwitter','uses'=>'PostController@getTwitterAccounts']); 
	Route::get('/posts/getfacebook',['as'=>'datatable.getfacebook','uses'=>'PostController@getFacebookAccounts']);


	Route::get('/users/switch/{id}','UsersController@switchAccount');
	

	Route::get('/users/dashboard',function() { return view('users.dashboard'); }); 
	Route::get('/users','UsersController@index'); 
	Route::get('/users/add','UsersController@add');
	Route::post('/users/add','UsersController@addUser'); 
	Route::get('/users/delete/{id}','UsersController@delete'); 
	Route::get('/users/profile/{id}','UsersController@profile');
	Route::post('/users/profile/{id}','UsersController@updateProfile');
	Route::get('/users/view/pdf/{id}','UsersController@viewPDF'); 

	
	Route::get('/tickets','TicketsController@index');
	Route::get('/tickets/{slug?}','TicketsController@show');
	Route::get('/tickets/edit/{slug?}','TicketsController@edit'); 
	Route::post('/tickets/edit/{slug?}','TicketsController@update');
	Route::get('/tickets/delete/{id}','TicketsController@delete'); 
	Route::get('/tickets/pdf/{slug?}','TicketsController@generatePDF');
	Route::get('/tickets/view/pdf/{slug?}','TicketsController@viewPDF');

	Route::get('/posts/add_post','PostController@showAddPost');  
	Route::post('/posts/add_post','PostController@savePost'); 
	Route::get('/posts/add','PostController@show');
	Route::post('/posts/add','PostController@addPost');  
	Route::get('/posts','PostController@index'); 
	Route::get('/posts/view/{id}','PostController@view');
	Route::get('/posts/edit/{id}','PostController@showEdit');
	Route::post('/posts/edit/{id}','PostController@updatePost');  
	Route::get('/posts/delete/{id}','PostController@delete');
	Route::post('/posts/approve/{id}','PostController@approvePost');      
	Route::get('/posts/add_twitter_acc','PostController@showTwitter');    
	Route::post('/posts/add_twitter_acc','PostController@addTwitterAccount');
	Route::get('/posts/add_fb_acc','PostController@showFacebookAcc');
	Route::post('/posts/add_fb_acc','PostController@addFacebookAccount');
	Route::get('/posts/post_to_fb','PostController@showFacebook');
	Route::post('/posts/post_to_fb','PostController@postToFacebook');
	Route::post('/posts/post_fb','PostController@postFB'); 
	Route::get('/posts/user_pages','PostController@getUserPages');   
	Route::get('/posts/all_fb_acc','PostController@showFacebookPage');
	Route::get('/posts/all_twitter_acc','PostController@showTwitterPage'); 

	Route::get('/posts/view_twitter_acc/{id}','PostController@viewTwitterAccount');
	Route::get('/posts/edit_twitter_acc/{id}','PostController@editTwitterAccount');
	Route::post('/posts/edit_twitter_acc/{id}','PostController@updateTwitterAccount'); 
	Route::get('/posts/delete_twitter_acc/{id}','PostController@deleteTwitterAccount');

	Route::get('/posts/view_fb_acc/{id}','PostController@viewFacebookAccount');
	Route::get('/posts/edit_fb_acc/{id}','PostController@editFacebookAccount');
	Route::post('/posts/edit_fb_acc/{id}','PostController@updateFacebookAccount');
	Route::get('/posts/delete_fb_acc/{id}','PostController@deleteFacebookAccount'); 

	Route::post('/social_media/upload','PostController@uploadSocialMedia'); 
	Route::get('/posts/post_to_insta','PostController@showInstaGram');
	Route::post('/posts/post_to_insta','PostController@postInstaGram');
});        	

Route::view('/privacy','users.privacy')->name('privacy');
Route::view('/terms','users.terms')->name('terms');
// Route::get('/privacy',function() { return view('users.privacy'); });
// Route::get('/terms',function() { return view('users.terms'); }); 
    
Route::get('auth/{provider}', 'UsersController@redirectToProvider');
Route::get('auth/{provider}/callback', 'UsersController@handleProviderCallback');

// Route::get('twitterUserTimeLine', 'PostController@twitterUserTimeLine');
Route::post('tweet', ['as'=>'post.tweet','uses'=>'PostController@tweet']);  

                 
  
