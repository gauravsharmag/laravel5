<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create(); 
        foreach( range(1,3) as $i ) { 
        	$title=$faker->sentence($nb=3);
        	DB::table('tickets')->insert([
        		'title'=>$title,
        		'content'=>$faker->text,
        		'slug'=>str_slug($title,'-'),
        		'status'=>0,
        		'created_at'=>$faker->dateTime($max = 'now'),
        		'updated_at'=>$faker->dateTime($max = 'now'),  
        	]); 
        }
    }
}
