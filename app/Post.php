<?php

namespace Laravel;

use Illuminate\Database\Eloquent\Model;  

class Post extends Model 
{
    public function post_images() {
    	return $this->hasMany('Laravel\PostImage');
    }
}
