<?php
namespace Laravel\Custom;
use Laravel\Model\AdminNotification; 
class Notification {

	public function notifyAdmin($args=array() ) {
		$adminNotify=new AdminNotification();
		$adminNotify->user_id=$args['user'];
		$adminNotify->type=$args['type'];
		$adminNotify->msg=$args['msg'];
		try {
			if($adminNotify->save()) {
				return true;
			}
			return false;
		} catch(Exception $e) {
			dd($e->getMessage());
		}
	}

	public function notifyUser($args=array() ) {
	}
	public function sendAdminMail($args=array() ) {

	}

}