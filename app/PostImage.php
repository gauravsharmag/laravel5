<?php

namespace Laravel;

use Illuminate\Database\Eloquent\Model;  

class PostImage extends Model
{
    public function post() {
    	return $this->belongsTo('Laravel\Post'); 
    }
}
