<?php

namespace Laravel\Console\Commands;

use Illuminate\Console\Command;

class CustomCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:custom';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing Laravel Commands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()  
    { 
        $this->info('I M testing larvel 5 commands');   
        dd('STOP');  
    }
}
