<?php

namespace Laravel\Console\Commands;

use Illuminate\Console\Command;
use Laravel\FacebookAccount;
use Laravel\Post;
use Laravel\PostImage; 
use Twitter;
use Facebook\Facebook;
use Facebook\GraphObject;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\FileUpload\FacebookFile;
use DB; 
use URL;

class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronJob:cronjob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post To Social Media';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()  
    {    
         // echo date('Y-m-d h:i:00'); 
         // echo PHP_EOL;
  
        // Get Posts for FB  
        $posts=Post::where('status',1)->where('post_type',1)
                  // ->orWhere('post_type',3)
                  ->where('scheduled_at',date('Y-m-d h:i:00'))
                  ->where('is_posted_on_fb',0)->with('post_images')->get()->toArray(); 
        if(count($posts)>0) { 
            foreach($posts as $p) {   
                $fbDetails=DB::table('facebook_accounts')->where('id',$p['facebook_account_id'])->first();
                if($fbDetails) {
                    // Post To FB   
                    $fb=new Facebook(array(
                            'app_id'=>$fbDetails->fb_app_id,
                            'app_secret'=>$fbDetails->fb_app_secret,
                            'default_graph_version'=>'v2.2'
                        ) 
                    );
                    // $fbIMGURL=asset('images/social_media/'.$p->image);  
                    if(!empty($p['post_images'])) {
                      $lastImg=end($p['post_images']);
                      $fbIMGURL=asset('images/social_media/'.$lastImg['image_name']); 
                    } 
                    $fbPostArr=[ 
                        'message'=>$p['title'],
                        'source'=> new FacebookFile($fbIMGURL) 
                    ];

                    try {
                          if($p['fb_post_type']==1) {    
                              // post to user timeline
                              $fbResponse=$fb->post('/me/photos', $fbPostArr,$fbDetails->fb_user_access_token); 
                              $json=json_decode($fbResponse->getBody()); 
                              if($json->post_id) {
                                  $this->info('Posted on '.$fbDetails->fb_user_name.' timeline');
                                  \DB::table('posts')->where('id',$p['id'])->update(['is_posted_on_fb'=>1]);
                              } else {
                                  $this->error('Sorry, cannot post on '.$fbDetails->fb_user_name.' timeline');
                              }       
                          } else {
                              // post to page
                              $fbResponse=$fb->get('/'.$fbDetails->fb_page_id.'/?fields=access_token',$fbDetails->fb_user_access_token);    
                              $pageToken = json_decode($fbResponse->getBody()); 
                              $response = $fb->post('/'.$fbDetails->fb_page_id.'/photos', $fbPostArr, $pageToken->access_token);
                              $json=json_decode($response->getBody());   
                              if($json->post_id) {
                                 $this->info('Posted on  '.$fbDetails->fb_page_name.' facebook page');
                                 \DB::table('posts')->where('id',$p['id'])->update(['is_posted_on_fb'=>1]);
                              } else {
                                  $this->error('Sorry, cannot post on '.$fbDetails->fb_page_name.' facebook page');
                              }  
                          }         
                    } catch(Facebook\Exceptions\FacebookResponseException $e) {
                        $this->error('Sorry,something went wrong,graph api returned an error: '.$e->getMessage());
                        // echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    } catch(Facebook\Exceptions\FacebookSDKException $e) {
                        $this->error('Sorry,something went wrong,Facebook SDK returned an error: '.$e->getMessage());
                        //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;   
                    }   
                }   
            }            
        } else {
            $this->info('Sorry, No posts found to post to facebook'); 
        } 

        // Get Posts For Twitter  
        $twPosts=Post::where('status',1)->where('post_type',2)
                    ->where('scheduled_at',date('Y-m-d h:i:00'))
                    ->where('is_posted_on_tw',0)->with('post_images')->get()->toArray(); 
        if(count($twPosts)>0 ) {
            // Post To Twitter
            foreach($twPosts as $t) {
                $twDetails=DB::table('twitter_accounts')->where('id',$t['twitter_account_id'])->first();
                if($twDetails) {  
                    $newTwitte = ['status' => $t['title']];
                    if(count($t['post_images'])>3) { 
                      for($p=0;$p<3;$p++) {
                        $uploaded_media = Twitter::uploadMedia(['media' =>file_get_contents(public_path().'/images/social_media/'.$t['post_images'][$p]['image_name'] ) ]);  
                        if(!empty($uploaded_media)) {
                            $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                        }
                      }
                    } else {
                      foreach($t['post_images'] as $img) {
                        $uploaded_media = Twitter::uploadMedia(['media' =>file_get_contents(public_path().'/images/social_media/'.$img['image_name'] ) ]);  
                        if(!empty($uploaded_media)) {
                            $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                        }
                      }     
                    } 
                    try {
                      Twitter::reconfig(['consumer_key' =>$twDetails->twitter_consumer_key, 
                                        'consumer_secret' => $twDetails->twitter_consumer_secret, 
                                        'token' => $twDetails->twitter_access_token,  
                                        'secret' => $twDetails->twitter_access_token_secret 
                                      ]);   
                      $twitter = Twitter::postTweet($newTwitte);
                      $this->info('Posted on  '.$twDetails->twitter_account.'\'s twitter account'); 
                      DB::table('posts')->where('id',$t['id'])->update(['is_posted_on_tw'=>1]);
                    } catch(Exception $e) {
                      $this->error('Sorry, something went wrong,error: '.$e->getMessage()); 
                    }       
                } 
            }         
        } else {     
            $this->info('Sorry, No posts found to post to twitter'); 
        }

        // Get Posts For Both facebook/twitter
        $bothPosts=Post::where('status',1)->where('post_type',3)
                  ->where('scheduled_at',date('Y-m-d h:i:00'))
                  ->where('is_posted_on_fb',0)
                  ->where('is_posted_on_tw',0)
                  ->with('post_images')->get()->toArray();   
        if(count($bothPosts)>0) {   
            foreach($bothPosts as $t) {
                $twDetails=DB::table('twitter_accounts')->where('id',$t['twitter_account_id'])->first();
                // post to twitter
                if($twDetails) {  
                    $newTwitte = ['status' => $t['title']];
                    if(count($t['post_images'])>3) { 
                      for($p=0;$p<3;$p++) {
                        $uploaded_media = Twitter::uploadMedia(['media' =>file_get_contents(public_path().'/images/social_media/'.$t['post_images'][$p]['image_name'] ) ]);  
                        if(!empty($uploaded_media)) {
                            $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                        }
                      }
                    } else {
                      foreach($t['post_images'] as $img) {
                        $uploaded_media = Twitter::uploadMedia(['media' =>file_get_contents(public_path().'/images/social_media/'.$img['image_name'] ) ]);  
                        if(!empty($uploaded_media)) {
                            $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                        }
                      }     
                    } 
                    try {
                      Twitter::reconfig(['consumer_key' =>$twDetails->twitter_consumer_key, 
                                        'consumer_secret' => $twDetails->twitter_consumer_secret, 
                                        'token' => $twDetails->twitter_access_token,  
                                        'secret' => $twDetails->twitter_access_token_secret 
                                      ]);   
                      $twitter = Twitter::postTweet($newTwitte);
                      $this->info('Posted on  '.$twDetails->twitter_account.'\'s twitter account'); 
                      DB::table('posts')->where('id',$t['id'])->update(['is_posted_on_tw'=>1]);
                    } catch(Exception $e) {
                      $this->error('Sorry, something went wrong,error: '.$e->getMessage()); 
                    }       
                } 
                $fbDetails=DB::table('facebook_accounts')->where('id',$t['facebook_account_id'])->first();
                // post to facebook
                if($fbDetails) {
                    // Post To FB   
                    $fb=new Facebook(array(
                            'app_id'=>$fbDetails->fb_app_id,
                            'app_secret'=>$fbDetails->fb_app_secret,
                            'default_graph_version'=>'v2.2'
                        ) 
                    );
                    // $fbIMGURL=asset('images/social_media/'.$p->image);  
                    if(!empty($t['post_images'])) {
                      $lastImg=end($t['post_images']);
                      $fbIMGURL=asset('images/social_media/'.$lastImg['image_name']); 
                    } 
                    $fbPostArr=[ 
                        'message'=>$t['title'],
                        'source'=> new FacebookFile($fbIMGURL) 
                    ];

                    try {
                          if($t['fb_post_type']==1) {    
                              // post to user timeline
                              $fbResponse=$fb->post('/me/photos', $fbPostArr,$fbDetails->fb_user_access_token); 
                              $json=json_decode($fbResponse->getBody()); 
                              if($json->post_id) {
                                  $this->info('Posted on '.$fbDetails->fb_user_name.' timeline');
                                  \DB::table('posts')->where('id',$t['id'])->update(['is_posted_on_fb'=>1]);
                              } else {
                                  $this->error('Sorry, cannot post on '.$fbDetails->fb_user_name.' timeline');
                              }       
                          } else {
                              // post to page
                              $fbResponse=$fb->get('/'.$fbDetails->fb_page_id.'/?fields=access_token',$fbDetails->fb_user_access_token);    
                              $pageToken = json_decode($fbResponse->getBody()); 
                              $response = $fb->post('/'.$fbDetails->fb_page_id.'/photos', $fbPostArr, $pageToken->access_token);
                              $json=json_decode($response->getBody());   
                              if($json->post_id) {
                                 $this->info('Posted on  '.$fbDetails->fb_page_name.' facebook page');
                                 \DB::table('posts')->where('id',$t['id'])->update(['is_posted_on_fb'=>1]);
                              } else {  
                                  $this->error('Sorry, cannot post on '.$fbDetails->fb_page_name.' facebook page');
                              }  
                          }         
                    } catch(Facebook\Exceptions\FacebookResponseException $e) {
                        $this->error('Sorry,something went wrong,graph api returned an error: '.$e->getMessage());
                        // echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    } catch(Facebook\Exceptions\FacebookSDKException $e) {
                        $this->error('Sorry,something went wrong,Facebook SDK returned an error: '.$e->getMessage());
                        //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;   
                    }   
                }   
            }  
        }         
    }  
}
