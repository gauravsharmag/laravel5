<?php

namespace Laravel;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model  
{
	protected $fillable=['title','content','slug','status','user_id'];

    public function user() {
    	return $this->belongsTo('Laravel\User');
    }

    public function getTitle() {
    	return $this->title;
    }
}
