<?php

namespace Laravel\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\User;

class NewUser extends Mailable
{
    use Queueable, SerializesModels;


    public $user;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
       $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {
        return $this->from('noreply@laravel.netgen.in')
                    ->subject('Thanks for regestering with us.')
                    // ->with(['user'=>$this->user])
                    ->attach(public_path().'/images/market.png',['as'=>'profilepic'])  
                    ->markdown('emails.users.new_user'); 

                        // 'firstKey'=>'firstVal',
                        // 'secondKey'=>'secondVal',
                        // 'firstColumn'=>$this->user->name,
                        // 'secondColumn'=>$this->user->email,
                        // 'thirdColumn'=>$this->user->role_id
    }
}
