<?php

namespace Laravel\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminCustomer   
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $roleID=array(1,2);
            if (!in_array(Auth::user()->role_id, $roleID)) { 
                 return redirect()->back()->with('error','Sorry,You are not authorized to access that location.');  
            } 
        } 
        return $next($request); 
    }
}
