<?php

namespace Laravel\Http\Controllers;
 
use Illuminate\Http\Request;
use Laravel\Http\Requests\TicketRequest;
use Laravel\Model\Ticket; 
use Laravel\Model\Testing;    
use Laravel\Deploy\Deploy;          
use PDF;
use DB;
// use Datatables;
use Yajra\Datatables\Datatables;   

class TicketsController extends Controller
{
 
    public function triggerHook(Request $req) {   
        $test=new Testing();
        $test->status='RESPONSE FROM GIT';
        $test->description='GIT WEBHOOK';

        $deploy = new Deploy('/home/laravel/public_html');
        $deploy->test();   
        // $deploy = new Deploy('/home/laravel/public_html'); 
        // $deploy->execute();        
        // if ( $_POST['payload'] ) {
        //   shell_exec( 'cd /srv/www/git-repo/ && git reset --hard HEAD && git pull' );
        // }
    }
	public function index() {
		$tickets=Ticket::all();  
		return view('tickets.index',compact('tickets')); 
	} 
    public function contact() {
    	return view('tickets.create');     
    }
    public function store(TicketRequest $request) {
        $title=rtrim($request->get('title'));
        $title=preg_replace('/[^A-Za-z0-9 \-]/', '', $title);
        // $chunks=explode(' ',$title);
        // $slug=implode('-',$chunks);
        $slug=str_slug($title, '-');        
    	$ticket=new Ticket(array(
    		'title'=>$title,
    		'content'=>$request->get('content'),
    		'slug'=>$slug
    	));
    	if($ticket->save()) {
    		return redirect('/contact')->with('success','Your Ticket has been created.');
    	}	
    } 
    public function show($slug) {
    	$ticket=Ticket::whereSlug($slug)->firstOrFail();
    	return view('tickets.view',compact('ticket')); 
    }
    public function edit($slug) { 
    	$ticket=Ticket::whereSlug($slug)->firstOrFail();
    	return view('tickets.edit',compact('ticket'));
    }
    public function update($slug,TicketRequest $req) {
    	$ticket=Ticket::whereSlug($slug)->firstOrFail();
        $title=rtrim($req->get('title'));
        $title=preg_replace('/[^A-Za-z0-9 \-]/', '', $title); 
        $slug=str_slug($title,'-');
    	$ticket->title=$title;
    	$ticket->content=$req->get('content');
        $ticket->slug=$slug;
    	if($req->get('status') !=null) {
    		$ticket->status=1;
    	} else {
    		$ticket->status=0;    
    	}
    	if($ticket->save()) {
    		return redirect('/tickets')->with('success','The Ticket updated successfully');
    	} else {
    		return redirect('/tickets/edit/$slug')->with('error','The Ticket could not be updated,Please try again!');
    	} 
    }
    public function delete($id) {
    	$ticket=Ticket::whereId($id)->firstOrFail();
        if($ticket) {
            if($ticket->delete()) {   
                return redirect('/tickets')->with('success','Ticket deleted successfully');
            } else { 
                return redirect()->back()->with('error','Ticket could not be deleted,Please try again!'); 
            }    
        } 
    }
    public function generatePDF($slug) {
        $ticket=Ticket::whereSlug($slug)->firstOrFail(); 
        $pdf=PDF::loadView('tickets.pdf.pdf',compact('ticket'));  
        return $pdf->download('invoice.pdf'); 
    } 
    public function viewPDF($slug) { 
        $ticket=Ticket::whereSlug($slug)->firstOrFail(); 
        $pdf=PDF::loadView('tickets.pdf.pdf',compact('ticket')); 
        return $pdf->stream();
    }
    public function gettickets(Request $request) {    
        $tickets = Ticket::select(['id', 'title', 'status','created_at','slug'])->get();   
        $str='';
        return Datatables::of($tickets)
            ->addColumn('action', function ($tic) { 
                $str='<a href="/tickets/edit/'.$tic->slug.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>';
                $str .='&nbsp;<a href="javascript:;" onclick="confirmDel('.$tic->id.')" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> </a>';
                $str .='&nbsp;<a href="/tickets/view/pdf/'.$tic->slug.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-download"></i></a>';
                return $str; 
            }) 
            ->editColumn('status',function($tic) {
                if($tic->status) {
                    return "<span class='btn btn-info'> Closed </span>";
                } else  {
                    return "<span class='btn btn-danger'> Pending </span>"; 
                }
            })
            ->editColumn('created_at',function($date) {
                return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
            })
            ->escapeColumns([])
            ->make(true);   
    }    
      
}
