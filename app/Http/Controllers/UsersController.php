<?php

namespace Laravel\Http\Controllers;

// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use ThrottlesLogins; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Http\Requests\UserRequest; 
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use Laravel\User;
use Laravel\UserAccess;
use Yajra\Datatables\Datatables; 
use Laravel\TwitterAccount;
use Laravel\FacebookAccount;  
use Socialite;
use PDF;
use Laravel\Notifications\NewUserRegistered;
use Illuminate\Support\Facades\Mail;
use Laravel\Mail\NewUser;
use Laravel\Events\UserLoggedIn;
   
class UsersController extends Controller 
{ 
	use AuthenticatesUsers;
	// use ThrottlesLogins; 

	protected $auth;
	protected $redirectTo = '/users/user_dashboard'; 
	    
    /**
    * lockoutTime
    *
    * @var
    */
	protected $lockoutTime;
	    
   /**
    * maxLoginAttempts
    *
    * @var
    */
	protected $maxLoginAttempts;


	public function __construct(Guard $auth) {
	    // $this->middleware('guest', ['except' => 'logout']); 
	    $this->auth = $auth;
	    $this->lockoutTime  = 200;    //lockout for 1 minute (value is in minutes)   
	    $this->maxLoginAttempts = 2;    //lockout after 5 attempts        
	}
	public function index() {
		$users=User::all();	
		return view('users.index',compact('users')); 
	}

	public function pagination_demo() {
		return User::paginate(10);
	}


	public function signin() {
		return view('users.signin');
	}
	public function signup() {
		return view('users.signup'); 
	}
	public function saveUser(Request $req) {
		$this->validate($req,[
				'name'=>'required|max:30',
				'email'=>'required|email|unique:users',
				'password'=>'required|min:4'
			],[
				'name.required'=>'Name is required.',
				'name.max'=>'Maximum 30 characters',
				'email.required'=>'Email is required.',
				'email.email'=>'Enter valid email.',
				'email.unique'=>'Email already exists.',
				'password.required'=>'Password is required.',
				'password.min'=>'Minimum 4 characters.'
			]
		);
		$user= new User();
		$user->name=$req->input('name'); 
		$user->email=$req->input('email');
		$user->password=$req->input('password');
		$user->role_id=2; 
		if($user->save()) {
			return redirect('/signin')->with('success','User created successfully,please login now'); 
		} else {
			return redirect('/signup')->with('error','User could not be created,please try again.'); 
		}
	}
	public function login(Request $req) {
		$this->validate($req,[
				'email'=>'required|email',   
				'password'=>'required'
			],[
				'email.required'=>__('Email is required.'),
				'email.email'=>__('Enter Valid Email.'),
				'password.required'=>__('Password is required.')
			]
		);

		if ($this->hasTooManyLoginAttempts($req)) {     
	        $this->fireLockoutEvent($req);	 
	        // dd($this->lockoutTime);
	        // dd($req);
	        // dd($this->sendLockoutResponse($req));
	        return $this->sendLockoutResponse($req);  
	    }

		if( Auth::attempt(['email'=>$req->input('email'),'password'=>$req->input('password') ]) ) {
			$user = Auth::user();
			// dd($req->session()->all() );
			// event(new UserLoggedIn(Auth::user()));      
			$this->clearLoginAttempts($req);
			if($user->role_id==1) {
				// return redirect('/users/dashboard')->with('user',$user->name);  
				return redirect()->intended('/users/dashboard'); 
			} else {
				// return redirect('/users/user_dashboard')->with('user',$user->name);  
				return redirect()->intended('/users/user_dashboard');
			} 
		} 
		$this->incrementLoginAttempts($req); 
		return redirect('/signin')->with('error',__('Invalid Email/Password,Please try again!')); 
	}
	protected function hasTooManyLoginAttempts(Request $request) {
	    return $this->limiter()->tooManyAttempts(
	         $this->throttleKey($request), $this->maxLoginAttempts, $this->lockoutTime
	    ); 
	}
	public function logout() {
		Auth::logout(); 
		return redirect('/signin');  
	} 
	public function profile($id) {
		$user=User::whereId($id)->firstOrFail();
		return view('users.profile',compact('user')); 
	}
	public function user($id) {
		$user=User::whereId($id)->firstOrFail();
		return view('users.user',compact('user'));  
	}  
	public function updateProfile($id,Request $req) {
		$this->validate($req,[ 
				'name'=>'required|max:30',
				'email'=>'required|email|unique:users,id',
				'file'=>'image|mimes:jpeg,png,jpg,gif|max:500'  // IN KB 
			],[  
				'name.required'=>'Name is required.',
				'name.max'=>'Maximum 30 characters',
				'email.required'=>'Email is required.',
				'email.email'=>'Enter valid email.',
				'email.unique'=>'Email already exists.',  
				'file.image'=>'Only jpeg,jpg,png and gif files are allowed.',
				'file.max'=>'Max file size is 500 KB.' 
			] 
		);
		$user=User::whereId($id)->firstOrFail();
		$oldImage=$user->image;
		$user->name=$req->get('name');
		$user->email=$req->get('email'); 
		if(isset($req->file)) {
			if($req->file('file')->getClientSize() >0) {
				$file=$req->file('file');
				$path=pathinfo($req->file('file')->getClientOriginalName());
				$imageName=time().'.'.$path['extension'];
				$destination=public_path().'/images/user_avatars/';
				$file->move($destination,$imageName);
				$user->image=$imageName;
				$FILETODELETE=public_path().'/images/user_avatars/'.$oldImage;
				\File::delete($FILETODELETE);   
			}
		}
		if($user->save()) {
			return redirect('/users')->with('success','Profile updated successfully.');
		} else { 
			return redirect()->back()->with('error','Profile could not be updated,Please try again!');
		} 
	}
	public function updateUserProfile($id,Request $req) {
		$this->validate($req,[ 
				'name'=>'required|max:30',
				'email'=>'required|email|unique:users,id',
				'file'=>'image|mimes:jpeg,png,jpg,gif|max:500'  // IN KB 
			],[  
				'name.required'=>'Name is required.',
				'name.max'=>'Maximum 30 characters',
				'email.required'=>'Email is required.',
				'email.email'=>'Enter valid email.',
				'email.unique'=>'Email already exists.',  
				'file.image'=>'Only jpeg,jpg,png and gif files are allowed.',
				'file.max'=>'Max file size is 500 KB.' 
			] 
		);
		$user=User::whereId($id)->firstOrFail();
		$oldImage=$user->image;
		$user->name=$req->get('name');
		$user->email=$req->get('email'); 
		if(isset($req->file)) {
			if($req->file('file')->getClientSize() >0) {
				$file=$req->file('file');
				$path=pathinfo($req->file('file')->getClientOriginalName());
				$imageName=time().'.'.$path['extension'];
				$destination=public_path().'/images/user_avatars/';
				$file->move($destination,$imageName);
				$user->image=$imageName;
				$FILETODELETE=public_path().'/images/user_avatars/'.$oldImage;
				\File::delete($FILETODELETE);   
			}
		}
		if($user->save()) {
			return redirect('/users/user_dashboard')->with('success','Profile updated successfully.');
		} else {    
			return redirect()->back()->with('error','Profile could not be updated,Please try again!');
		} 
	}
	public function add() {
		$fbUsers=FacebookAccount::pluck('fb_user_name','id');
		$twUsers=TwitterAccount::pluck('twitter_account','id');
		return view('users.add',compact('fbUsers','twUsers'));    
	} 
	public function addUser(Request $req) { 
		if(isset($req->post_type)) {
		    if($req->post_type==1) {
		    	$this->validate($req,[
		    		'name'=>'required',
		    		'email'=>'required|email|unique:users',
		    		'password'=>'required',
		    		'post_type'=>'required',
		    		'fb_user'=>'required'
		    	]);
		    } else if($req->post_type==2) {
		        $this->validate($req,[
		        	'name'=>'required',
		        	'email'=>'required|email|unique:users',
		        	'password'=>'required',
		            'post_type'=>'required',
		            'tw_user'=>'required'
		        ]);
		    } else if($req->post_type==3) {
		        $this->validate($req,[
		        	'name'=>'required',
		        	'email'=>'required|email|unique:users',
		        	'password'=>'required',
		            'post_type'=>'required',
		            'fb_user'=>'required',
		            'tw_user'=>'required'
		        ]);
		    } else {     
		        $this->validate($req,[
		            'name'=>'required',
		    		'email'=>'required|email|unique:users',
		    		'password'=>'required',
		    		'post_type'=>'required',
		        ]);
		    }   
		} else {
			$this->validate($req,[
				'name'=>'required',
				'email'=>'required|email|unique:users',
				'password'=>'required',
				'post_type'=>'required' 
			]);  
		}
		$user=new User();
		$user->name=$req->get('name');
		$user->email=$req->get('email');
		$user->password=$req->get('password');
		$user->role_id=$req->get('role_id');   
		if($user->save()) {
			$userAccess=new UserAccess();
			$userAccess->user_id=$user->id;
			$userAccess->post_type=$req->get('post_type');
			$userAccess->facebook_account_id=$req->get('fb_user_ids');
			$userAccess->twitter_account_id=$req->get('tw_user_ids'); 
			$userAccess->fb_post_type=$req->get('fb_post_type'); 
			if($userAccess->save()) {
				// $user->notify(new NewUserRegistered($user));
				Mail::to($user->email)->send(new NewUser($user));
				return redirect('/users')->with('success','User added successfully. ');
			} else {
				return redirect()->back()->with('error','User access could not be added,Please try again.');
			}
		} else {
			return redirect()->back()->with('error','User could not be added,Please try again.');
		}
	}
	public function delete($id) {   
		$user=User::whereId($id)->firstOrFail();
		if($user) {
			if($user->delete()) {
				return redirect('/users')->with('success','User deleted successfully.');
			} else {
				return redirect()->back()->with('error','Sorry, user could not be deleted,please try again!');
			}
		}
	} 
	public function getUsers(Request $req) {
		$users = User::select(['id', 'name', 'role_id','email','created_at'])->get();   
		$str='';
		return Datatables::of($users)
		    ->addColumn('action', function ($user) {    
		       $str='<a href="/users/profile/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>'; 
		       $str .='&nbsp;<a id="deleteUser"  onclick="confirmDel('.$user->id.');" href="javascript:;" class="btn btn-xs btn-danger"> <i class="glyphicon glyphicon-trash"></i> </a>'; 
		       $str .='&nbsp;<a target="_blank" href="/users/view/pdf/'.$user->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-download"></i></a>';  
		       $str .='&nbsp;<a id="switchUser"  onclick="confirmSwitch('.$user->id.');" href="javascript:;" class="btn btn-xs btn-success"> Switch</a>'; 
		       // $str .='&nbsp;<a href="/users/switch/'.$user->id.'" class="btn btn-xs btn-info">Switch</a>';    
		       return $str;  
		    })   
		    ->editColumn('role_id',function($user) {
		        if($user->role_id==1) {
		            return "<span class='btn btn-info'> Admin </span>";
		        } else  {
		            return "<span class='btn btn-danger'> User </span>"; 
		        }
		    })
		    ->editColumn('created_at',function($date) {
		        return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
		    })
		    ->escapeColumns([])
		    ->make(true); 
	}
	public function switchAccount($id) {
		if($id) {
			session(['back_id' => Auth::user()->id ]); 
			$newUser=User::where('id',$id)->first();
			if(Auth::loginUsingId($newUser->id)) {
				if(Auth::user()->role_id==2) {
					return redirect('/users/user_dashboard')->with('success','Account switched successfully.');
				} 
			} else {
				session()->pull('back_id');
				return redirect()->back()->with('error','Sorry, Error in switchning account, please try again!');
			}
		}
	}
	public function switchAccountBack($id) {
		if($id) {
 			if($id==session('back_id')) {
 				$newUser=User::where('id',$id)->first();
 				if($newUser) {
 					if(Auth::loginUsingId($newUser->id)) {
 						session()->pull('back_id');
 						if(Auth::user()->role_id==1) {
 							return redirect('/users/dashboard')->with('success','Account switched successfully.');			
 						}
 					} else { 
 						session()->pull('back_id');
 						return redirect()->back()->with('error','Sorry, something went wrong while switchning account,please try again!');			
 					}
 				} else {
 					return redirect()->back()->with('error','Sorry, user doesn\'t exists');
 				}
 			} else {
 				return redirect()->back()->with('error','Sorry, back id is not correct!');
 			}
		}
	}  

	public function viewPDF($id) { 
	    $user=User::whereId($id)->firstOrFail(); 
	    if($user->role_id==1) {
	    	$user->role_id='Admin';
	    } else {
	    	$user->role_id='User';
	    } 
	    $pdf=PDF::loadView('users.pdf.pdf',compact('user')); 
	    return $pdf->stream(); 
	}
	public function showChangePassword() {
		return view('users.change_password');
	}
	public function changePassword(Request $req) {
		$this->validate($req, [
	        'password'=> 'required',
	        'confirm_password' => 'required|same:password'
		]); 
		$user=User::find(Auth::user()->id);
		$user->password=$req->get('password');
		if($user->save()) {
			return redirect()->back()->with('success','Password updated successfully.');
		} else {
			return redirect()->back()->with('error','Sorry,Password could not be updated,please try again!');
		} 
	}

	/**
	* Redirect the user to the OAuth Provider.
	*
	* @return Response 
	*/
	public function redirectToProvider($provider) {
		return Socialite::driver($provider)->redirect();
	}

	/**
    * Obtain the user information from provider.  Check if the user already exists in our
    * database by looking up their provider_id in the database.
    * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
    * redirect them to the authenticated users homepage.
    *
    * @return Response  
    */
    public function handleProviderCallback($provider){
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo)->with('success','LoggedIn successfully with  '.ucfirst($provider));
    }   
    /**
    * If a user has registered before using social auth, return the user
    * else, create a new user object.
    * @param  $user Socialite user object
    * @param $provider Social auth provider
    * @return  User
    */
    public function findOrCreateUser($user, $provider) {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;   
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id 
        ]);  
    }

}
