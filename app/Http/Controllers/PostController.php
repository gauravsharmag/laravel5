<?php

namespace Laravel\Http\Controllers;
use Laravel\InstaGramPost;
use Laravel\Post; 
use Laravel\PostImage;
use Laravel\UserAccess;   
use Laravel\TwitterAccount;
use Laravel\FacebookAccount;   
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Twitter;
use File;  
use DB;
use Facebook\Facebook;
use Facebook\GraphObject;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\FileUpload\FacebookFile;
use Illuminate\Support\Facades\Auth;
    
class PostController extends Controller
{ 
    public function uploadSocialMedia(Request $req) {
        $res=array();  
        if(isset($req->userImage)) {
            if($req->file('userImage')->getClientSize() >0) {
                $file=$req->file('userImage');
                $path=pathinfo($req->file('userImage')->getClientOriginalName());
                $imageName=time().'.'.$path['extension'];
                $destination=public_path().'/images/social_media/';
                if($file->move($destination,$imageName)){
                    $res['success']=true;
                    $res['imagePath']='/images/social_media/';
                    $res['image']=$imageName;
                    $res['completePath']=$_SERVER['HTTP_HOST'].'/images/social_media/'.$imageName;
                }    
                echo json_encode($res); 
            }
        }
    }   
    public function show() {

        // dd($_SERVER['HTTP_HOST']);
        // $fb=new Facebook(array(
        //     'app_id'=>'186640175225086',
        //     'app_secret'=>'0e571a3cf70bc510183f96844d7b96ec',
        //     'default_graph_version'=>'v2.2',
        // ));

        // $linkData = [
        //   'message'=>'Custom Message For Testing...',
        //   'name'=>'Post From Laravel 5', 
        //   'link' => 'https://homestay.netgen.in/',
        //   'description'=>'Some Long Description Goes Here...',
        //   'picture'=>'http://static.dezeen.com/uploads/2013/03/dezeen_Sergio-concept-car-by-Pininfarina_ss_4.jpg' 
        //   ];
  
        // try {
            
            // $response = $fb->post('/{page-id}/photos', $data, $accessToken);
            // $response = $fb->get('/137559119372?fields=access_token','EAACpv41YWP4BAIl3K6VgsPv09S7tMxT6muG5CZBCBHZCeV8INsCDBZBDAe1odjlo2ugDOCOkBOaGCq8ZAcvNVD3mtBwmmuyZCff7rTGAt9L31QJLVuaEIxSEgURjdNiwqNPg1bZCoiV6TrMSrZBNsCCxSXcTxivdfmm2IX31hN8ipTEpFAlZCom5z1CbIhFjuHsZD');    
            
            // dd($response); 

            //$json = json_decode($response->getBody());
            // dd($json); 
            //$page_token = $json->id;
            //dd($page_token);
            // dd($page_token);   
            // $response = $fb->post('/'.$pageId.'/feed', $fbData, $page_token);


          // Returns a `Facebook\FacebookResponse` object   
          
          // $response = $fb->post('/me/feed', $linkData, 'EAACpv41YWP4BAKAsZCmEGLXr8OXjDJbKEzYXECulPdLcuaxZBJwm87JLBvyzADTnLn0XmKlNB83YrYt2BvVNOELY0HMMxo34kJ1x4PJTArZCCfeAF5jd3G5FHvY6SUR5YOtvG8pxj71bkgkjf86GV7GV9ZBG60EZD');
          

            // $response = $fb->get('/me');
            // $posts_request = $fb->get('/me','EAACpv41YWP4BAKAsZCmEGLXr8OXjDJbKEzYXECulPdLcuaxZBJwm87JLBvyzADTnLn0XmKlNB83YrYt2BvVNOELY0HMMxo34kJ1x4PJTArZCCfeAF5jd3G5FHvY6SUR5YOtvG8pxj71bkgkjf86GV7GV9ZBG60EZD');

        // $response = $fb->get('/me?fields=id,name,email', 'EAACpv41YWP4BAKAsZCmEGLXr8OXjDJbKEzYXECulPdLcuaxZBJwm87JLBvyzADTnLn0XmKlNB83YrYt2BvVNOELY0HMMxo34kJ1x4PJTArZCCfeAF5jd3G5FHvY6SUR5YOtvG8pxj71bkgkjf86GV7GV9ZBG60EZD');
        // $pic=$fb->get('/me/picture?redirect=false&height=300','EAACpv41YWP4BAKAsZCmEGLXr8OXjDJbKEzYXECulPdLcuaxZBJwm87JLBvyzADTnLn0XmKlNB83YrYt2BvVNOELY0HMMxo34kJ1x4PJTArZCCfeAF5jd3G5FHvY6SUR5YOtvG8pxj71bkgkjf86GV7GV9ZBG60EZD');

        // } catch(Facebook\Exceptions\FacebookResponseException $e) {
        //   echo 'Graph returned an error: ' . $e->getMessage();
        //   exit;
        // } catch(Facebook\Exceptions\FacebookSDKException $e) {
        //   echo 'Facebook SDK returned an error: ' . $e->getMessage();
        //   exit;
        // }
        // dd($posts_request);
        // $posts_response = $posts_request->getGraphEdge();
        // $user = $response->getGraphUser();
        // echo  "<pre>";
        // print_r( $pic);
        // dd($user); 
        // echo 'Name: ' . $user['name'];

        // dd($posts_response);  

        // $graphNode = $response->getGraphNode();
        // echo 'Posted with id: ' . $graphNode['id']; 

         
        $accounts=TwitterAccount::pluck('twitter_account','id'); 
        return view('posts.add',compact('accounts'));  
    }      
    public function showFacebook() {
        // $fbUserTokens=FacebookAccount::pluck('fb_user_name','fb_user_access_token'); 
        // $fbPageIDs=FacebookAccount::pluck('fb_page_name','fb_page_id'); 
        // $fbAppIDs=FacebookAccount::pluck('fb_app_id','fb_app_id');
        // $fbAppSecrets=FacebookAccount::pluck('fb_app_secret','fb_app_secret');  
        // return view('posts.post_to_fb',compact('fbUserTokens','fbPageIDs','fbAppIDs','fbAppSecrets'));    
        $fbUserTokens=FacebookAccount::pluck('fb_user_name','id'); 
        $fbPageIDs=FacebookAccount::pluck('fb_page_name','fb_page_id');  
        $fbAppIDs=FacebookAccount::pluck('fb_app_id','fb_app_id'); 
        $fbAppSecrets=FacebookAccount::pluck('fb_app_secret','fb_app_secret');  

        return view('posts.post_to_fb',compact('fbUserTokens','fbPageIDs','fbAppIDs','fbAppSecrets'));    
    }
    public function addPost(Request $req) {
        $post=new Post();
        $post->title=$req->get('title'); 
        $post->content=$req->get('content');
        if($post->save()) {
            return redirect('/posts')->with('success','Post Added Successfully'); 
        } 
    } 
    public function view($id) {
        $post=Post::whereId($id)->firstOrFail(); 
        if($post) {
            if($post->facebook_account_id) { 
                $fb=FacebookAccount::whereId($post->facebook_account_id)->select(['fb_user_name'])->first(); 
                if($fb) {
                    $post->facebook_account_name=$fb->fb_user_name;
                }
            }
            if($post->twitter_account_id) {
                $tw=TwitterAccount::whereId($post->twitter_account_id)->select(['twitter_account'])->first();
                if($tw) { 
                    $post->twitter_account_name=$tw->twitter_account;
                }
            }
            if($post->scheduled_at) {
                $post->scheduled_at=date('d-m-Y H:i',strtotime($post->scheduled_at));   
            }   
            $postImg=Post::find($id)->post_images;    
        }  
        return view('posts.view',compact('post','postImg'));   
    }
    public function index() {
        $posts=Post::all(); 
        return view('posts.index',compact('posts')); 
    }  
    public function getPosts(Request $request) {    
        $posts = Post::select(['id', 'title','status','is_posted_on_fb','is_posted_on_tw','created_at'])->get();    
        $str='';
        return Datatables::of($posts)
            ->addColumn('action', function ($post) { 
                $str ='&nbsp;<a href="/posts/view/'.$post->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i></a>'; 
                $str .='&nbsp;<a href="/posts/edit/'.$post->id.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i></a>'; 
                $str .='&nbsp;<a href="javascript:;" postid="'.$post->id.'" onclick="confirmDel('.$post->id.');"  id="postDeleteBTN" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';      
                return $str;  
            }) 
            ->editColumn('title',function($title) {
                return str_limit($title->title, $limit = 100, $end = '...');
            }) 
            ->editColumn('created_at',function($date) {
                return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
            }) 
            ->editColumn('status',function($status) {
                if($status->status) {
                    return "<span class='btn btn-success'> Approved </span>";
                } else {
                    return "<span class='btn btn-danger'> Not-Approved </span>";
                }
            })
            ->editColumn('is_posted_on_fb',function($fb) {
                if($fb->is_posted_on_fb) {
                    return "<span class='btn btn-success'> Yes </span>";
                } else {
                    return "<span class='btn btn-danger'> No </span>";
                }
            })
            ->editColumn('is_posted_on_tw',function($tw) {
                if($tw->is_posted_on_tw) {
                    return "<span class='btn btn-success'> Yes </span>";
                } else {
                    return "<span class='btn btn-danger'> No </span>"; 
                } 
            })   
            ->escapeColumns([]) 
            ->make(true);   
    }
    public function tweet(Request $request) {
        $this->validate($request, [
                'tweet' => 'required|max:140'
        ]); 
        $twitterAccount=TwitterAccount::whereId($request->get('twitter_account'))->firstOrFail();  
        $newTwitte = ['status' => $request->tweet];
        if(!empty($request->images)){
            if(count($request->images) <=4 ) {
                foreach ($request->images as $key => $value) {
                    $uploaded_media = Twitter::uploadMedia(['media' => File::get($value->getRealPath())]);
                    if(!empty($uploaded_media)){
                        $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                    }
                }
            } else {
                return back()->with('error','Sorry,Max. 4 images are allowed.');  
            }
        }   
        Twitter::reconfig(['consumer_key' =>$twitterAccount->twitter_consumer_key, 
                          'consumer_secret' => $twitterAccount->twitter_consumer_secret, 
                          'token' => $twitterAccount->twitter_access_token,  
                          'secret' => $twitterAccount->twitter_access_token_secret 
                        ]); 
        $twitter = Twitter::postTweet($newTwitte);
        return back()->with('success','Posted to Twitter Successfully');    
    }
    public function postToTwitter($id) {
        $post=Post::whereId($id)->firstOrFail();

        $newTwitte = ['status' => $post->title];
        if(!empty($request->images)){
            foreach ($request->images as $key => $value) {
                $uploaded_media = Twitter::uploadMedia(['media' => File::get($value->getRealPath())]);
                if(!empty($uploaded_media)){
                    $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                }
            }
        }

        $twitter = Twitter::postTweet($newTwitte);
        return back()->with('success','Posted to Twitter Successfully');  
    }
    public function showTwitter() {  
        return view('posts.add_twitter_acc');   
    }
    public function addTwitterAccount(Request $req) {
      $this->validate($req,[
            'twitter_account'=>'required',
            'twitter_consumer_key'=>'required',
            'twitter_consumer_secret'=>'required',
            'twitter_access_token'=>'required',
            'twitter_access_token_secret'=>'required'
      ]);
      $twitterAcc=new TwitterAccount();
      $twitterAcc->twitter_account=$req->get('twitter_account');
      $twitterAcc->twitter_consumer_key=$req->get('twitter_consumer_key');
      $twitterAcc->twitter_consumer_secret=$req->get('twitter_consumer_secret');
      $twitterAcc->twitter_access_token=$req->get('twitter_access_token');
      $twitterAcc->twitter_access_token_secret=$req->get('twitter_access_token_secret');
      if($twitterAcc->save()) {
            return redirect('/posts/all_twitter_acc')->with('success','Account added successfully.');
      } else {
        return redirect()->back()->with('error','Sorry,Account could not be added,please try again.');
      }
    }
    public function showFacebookAcc() {
        $fbUserTokens=FacebookAccount::pluck('fb_user_access_token','fb_user_access_token'); 
        $fbPageIDs=FacebookAccount::pluck('fb_page_id','fb_page_id');  
        return view('posts.add_fb_acc',compact('fbUserTokens','fbPageIDs'));   
    }
    public function addFacebookAccount(Request $req) { 
        $this->validate($req,[  
              'fb_user_name'=>'required',
              'fb_app_id'=>'required',
              'fb_app_secret'=>'required',
              'fb_user_access_token'=>'required',
              'fb_page_id'=>'required',
              'fb_page_name'=>'required'
        ]);    
        $fbAcc=new FacebookAccount();
        $fbAcc->fb_user_name=$req->get('fb_user_name');
        $fbAcc->fb_app_id=$req->get('fb_app_id');   
        $fbAcc->fb_app_secret=$req->get('fb_app_secret');     
        $fbAcc->fb_user_access_token=$req->get('fb_user_access_token');
        $fbAcc->fb_page_id=$req->get('fb_page_id');    
        $fbAcc->fb_page_name=$req->get('fb_page_name');     
        if($fbAcc->save()) { 
           return redirect('/posts/all_fb_acc')->with('success','Account added successfully.');
        } else {
          return redirect()->back()->with('error','Sorry,Account could not be added,please try again.');
        }
    } 
    function base64_to_jpeg( $base64_string, $output_file ) {
      $ifp = fopen( $output_file, "wb" ); 
      fwrite( $ifp, base64_decode( $base64_string) ); 
      fclose( $ifp ); 
      return( $output_file ); 
    }  
    public function postToFacebook(Request $req) {
        $this->validate($req, [
                'facebook_post' => 'required|max:200',
                'userImage'=>'required|image|mimes:jpeg,png,jpg,gif|max:2048' // In KB
        ]); 
        $fb=new Facebook(array(
            'app_id'=>$req->get('fb_app_id'),
            'app_secret'=>$req->get('fb_app_secret'), 
            'default_graph_version'=>'v2.2',     
        )); 
        $imageName='';
        if(isset($req->userImage)) { 
            if($req->file('userImage')->getClientSize() >0) {
                $file=$req->file('userImage');
                $path=pathinfo($req->file('userImage')->getClientOriginalName());
                $imageName=time().'.'.$path['extension'];  
                $destination=public_path().'/images/social_media/'; 
                $file->move($destination,$imageName);
            }  
        }

        $url = asset('images/social_media/'.$imageName);    
        // $url = asset('images/home_bg.jpg');  
        $linkData = [  
          'message'=>$req->get('facebook_post'), 
          'source'=> new FacebookFile($url)            
        ];  
        try {
            if($req->get('fb_post_type')==2) {
                $response=$fb->get('/'.$req->get('fb_page_id').'/?fields=access_token',$req->get('fb_user_access_token'));
                $json = json_decode($response->getBody());
                $page_token = $json->access_token;  
                $response = $fb->post('/'.$req->get('fb_page_id').'/photos', $linkData, $page_token);
                unlink(asset('images/social_media/'.$imageName ));                     
                return redirect()->back()->with('success','Posted To Facebook.');  
            } else {  
                $request = $fb->post('/me/photos', $linkData,$req->get('fb_user_access_token'));
                unlink(asset('images/social_media/'.$imageName ));  
                return redirect()->back()->with('success','Posted To Facebook.'); 
            } 
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }
    }
    public function getUserPages(Request $req) {
        $res=array();
        if($req->query('user')) {
            $page=FacebookAccount::select(['id','fb_page_id','fb_page_name'])->whereId($req->query('user'))->firstOrFail();
            if($page) {
                $res['page']=$page;
            }
            echo json_encode($res);
        }
    } 
    public function postFB(Request $req) {
        $this->validate($req, [
                'facebook_post' => 'required|max:200',
                'fb_user_access_token'=>'required',
                'userImage'=>'required|image|mimes:jpeg,png,jpg,gif|max:2048' // In KB
        ]); 
        if($req->get('fb_user_access_token')) {
            $fbDetails=FacebookAccount::whereId($req->get('fb_user_access_token'))->firstOrFail();
            $fbIMGURL=''; 
            if($fbDetails) {
                $fb=new Facebook(array(
                        'app_id'=>$fbDetails->fb_app_id,
                        'app_secret'=>$fbDetails->fb_app_secret,
                        'default_graph_version'=>'v2.2'
                    )
                );
                if(isset($req->userImage)) {
                    if($req->file('userImage')->getClientSize()>0) {
                        $file=$req->file('userImage');
                        $pathInfo=pathinfo($file->getClientOriginalName());
                        $pic=time().'.'.$pathInfo['extension'];
                        $file->move(public_path().'/images/social_media/',$pic);
                        $fbIMGURL=asset('images/social_media/'.$pic);
                    }
                }
                $fbPostArr=[
                    'message'=>$req->get('facebook_post'),
                    'source'=> new FacebookFile($fbIMGURL)
                ];
                try {
                    if($req->get('fb_post_type')==1) {   
                        // post to user timeline
                        $fbResponse=$fb->post('/me/photos', $fbPostArr,$fbDetails->fb_user_access_token); 
                        // unlink(public_path().'/images/social_media/',$pic); 
                        $json=json_decode($fbResponse->getBody());
                        if($json->post_id) {
                            return redirect()->back()->with('success','Posted To Facebook With PostID:- '.$json->post_id);
                        } else {
                            return redirect()->back()-with('error','Sorry, something went wrong,please try again!');
                        }     
                    } else {
                        // post to page
                        $fbResponse=$fb->get('/'.$fbDetails->fb_page_id.'/?fields=access_token',$fbDetails->fb_user_access_token);    
                        $pageToken = json_decode($fbResponse->getBody()); 
                        $response = $fb->post('/'.$fbDetails->fb_page_id.'/photos', $fbPostArr, $pageToken->access_token);
                        $json=json_decode($response->getBody());   
                        // unlink(public_path().'/images/social_media/',$pic); 
                        if($json->post_id) {
                            return redirect()->back()->with('success','Posted To Facebook With PostID:- '.$json->post_id);
                        } else {
                            return redirect()->back()-with('error','Sorry, something went wrong,please try again!');
                        }  
                    }   
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                  echo 'Graph returned an error: ' . $e->getMessage();
                  exit;
              } catch(Facebook\Exceptions\FacebookSDKException $e) {
                  echo 'Facebook SDK returned an error: ' . $e->getMessage();
                  exit;
              }
            }  
        }   
    }  
    public function showAddPost() {
        // $postImgs=Post::where('id',49)->with('post_images')->first()->toArray();
        // dd($postImgs['post_images']);  
        $fbUsers=FacebookAccount::pluck('fb_user_name','id');
        $twUsers=TwitterAccount::pluck('twitter_account','id');
        return view('posts.add_post',compact('fbUsers','twUsers'));
    }     
    public function savePost(Request $req) {
        if(isset($req->post_type)) {
            if($req->post_type==1) {
                $this->validate($req,[
                    'post_type'=>'required',
                    'fb_user'=>'required',
                    'post_title'=>'required|max:300',
                    'post_content'=>'required',
                    'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048'  // size in KB  
                ]);
            } else if($req->post_type==2) {
                $this->validate($req,[
                    'post_type'=>'required',
                    'tw_user'=>'required',
                    'post_title'=>'required|max:300',
                    'post_content'=>'required',
                    'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048'  // size in KB  
                ]);
            } else if($req->post_type==3) {
                $this->validate($req,[
                    'post_type'=>'required',
                    'fb_user'=>'required',
                    'tw_user'=>'required',
                    'post_title'=>'required|max:300',
                    'post_content'=>'required',
                    'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048'  // size in KB  
                ]);
            } else {     
                $this->validate($req,[
                    'post_type'=>'required',
                    'post_title'=>'required|max:300',
                    'post_content'=>'required',
                    'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048'  // size in KB  
                ]);
            }   
        } else {
            $this->validate($req,[
                'post_type'=>'required',
                'post_title'=>'required|max:300', 
                'post_content'=>'required',
                'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048' 
            ]);
        }
        $IMGNAMEARR=array(); 
        if(count($req->post_images) <=3 ) {
            if(isset($req->post_images)) {
                foreach($req->file('post_images') as $f ) {
                    if($f->getClientSize()>0) {
                        $path=pathinfo($f->getClientOriginalName());
                        $imgName=uniqid().'.'.$path['extension'];
                        $f->move(public_path().'/images/social_media/',$imgName);
                        $IMGNAMEARR[]=$imgName;
                    }
                }
            }
            $post=new Post();
            $post->title=$req->get('post_title');
            $post->content=$req->get('post_content');
            $post->facebook_account_id=$req->get('fb_user');
            $post->twitter_account_id=$req->get('tw_user'); 
            $post->fb_post_type=$req->get('fb_post_type');
            $post->post_type=$req->get('post_type');
            $post->user_id=Auth::user()->id;  
            if($req->get('scheduled_at') !='') { 
                $post->scheduled_at=date('Y-m-d H:i',strtotime($req->get('scheduled_at')));   
            } 
            if($post->save()) {
                if(isset($req->post_images)) {
                    if(!empty($IMGNAMEARR)) {
                        foreach($IMGNAMEARR as $f) {
                            $postImage=new PostImage();
                            $postImage->post_id=$post->id;
                            $postImage->image_name=$f;
                            $postImage->save();
                        }   
                    }
                }  
                return redirect('/posts')->with('success','Post added successfully.');   
            } else {
                return redirect()->back()->with('error','Post could not be added,please try again.');
            }
        } else {
            return redirect()->back()->with('error','Sorry, max. 3 images are allowed to upload.');
        } 
    }  
    public function facebookPost($id) {  
        // Get Posts for FB  
        // $posts=Post::where('status',1)->where('is_posted_on_fb',0)->with('post_images')->get()->toArray(); 
        $posts=Post::where('id',$id)->with('post_images')->get()->toArray(); 
        if(count($posts)>0) { 
            foreach($posts as $p) {   
                $fbDetails=DB::table('facebook_accounts')->where('id',$p['facebook_account_id'])->first();
                if($fbDetails) {
                    // Post To FB   
                    $fb=new Facebook(array(
                            'app_id'=>$fbDetails->fb_app_id,
                            'app_secret'=>$fbDetails->fb_app_secret,
                            'default_graph_version'=>'v2.2'
                        ) 
                    );
                    // $fbIMGURL=asset('images/social_media/'.$p->image);  
                    if(!empty($p['post_images'])) {
                      $lastImg=end($p['post_images']);
                      $fbIMGURL=asset('images/social_media/'.$lastImg['image_name']); 
                    } 
                    $fbPostArr=[ 
                        'message'=>$p['title'],
                        'source'=> new FacebookFile($fbIMGURL) 
                    ];

                    try {
                          if($p['fb_post_type']==1) {    
                              // post to user timeline
                              $fbResponse=$fb->post('/me/photos', $fbPostArr,$fbDetails->fb_user_access_token); 
                              $json=json_decode($fbResponse->getBody()); 
                              if($json->post_id) {
                                  \DB::table('posts')->where('id',$p['id'])->update(['is_posted_on_fb'=>1]);
                                  // $this->info('Posted on '.$fbDetails->fb_user_name.' timeline');
                                    redirect('/posts/')->with('success','Posted on '.$fbDetails->fb_user_name.' timeline');
                              } else {
                                  // $this->info('Sorry, cannot post on '.$fbDetails->fb_user_name.' timeline');
                                 redirect('/posts/')->with('error','Sorry, cannot post on '.$fbDetails->fb_user_name.' timeline');
                              }       
                          } else {
                              // post to page
                              $fbResponse=$fb->get('/'.$fbDetails->fb_page_id.'/?fields=access_token',$fbDetails->fb_user_access_token);    
                              $pageToken = json_decode($fbResponse->getBody()); 
                              $response = $fb->post('/'.$fbDetails->fb_page_id.'/photos', $fbPostArr, $pageToken->access_token);
                              $json=json_decode($response->getBody());   
                              if($json->post_id) {
                                 // $this->info('Posted on  '.$fbDetails->fb_page_name.' facebook page');
                                 \DB::table('posts')->where('id',$p['id'])->update(['is_posted_on_fb'=>1]);
                                  redirect('/posts/')->with('success','Posted on '.$fbDetails->fb_page_name.' facebook page');
                              } else {
                                  // $this->info('Sorry, cannot post on '.$fbDetails->fb_page_name.' facebook page');
                                 redirect('/posts/')->with('error','Sorry, cannot post on '.$fbDetails->fb_page_name.' timeline'); 
                              }  
                          }         
                    } catch(Facebook\Exceptions\FacebookResponseException $e) {
                        //$this->info('Sorry,something went wrong,graph api returned an error: '.$e->getMessage());
                        // echo 'Graph returned an error: ' . $e->getMessage();
                        redirect('/posts/')->with('error','Sorry,something went wrong,graph api returned an error: '.$e->getMessage()); 
                        // exit;
                    } catch(Facebook\Exceptions\FacebookSDKException $e) {
                        // $this->info('Sorry,something went wrong,Facebook SDK returned an error: '.$e->getMessage());
                        //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        redirect('/posts/')->with('error','Sorry,something went wrong,Facebook SDK returned an error: '.$e->getMessage()); 
                        // exit;   
                    }   
                }   
            }           
        } else {
            // $this->info('Sorry, No posts found to post to facebook'); 
            return redirect()->back()->with('error','Sorry no posts found to post to facebook.');
        } 
    }  
    public function twitterPost($id) {
        $twPosts=Post::where('id',$id)->with('post_images')->get()->toArray(); 
        if(count($twPosts)>0 ) {
            // Post To Twitter
            foreach($twPosts as $t) {
                $twDetails=DB::table('twitter_accounts')->where('id',$t['twitter_account_id'])->first();
                if($twDetails) {  
                    $newTwitte = ['status' => $t['title']];
                    if(count($t['post_images'])>3) { 
                      for($p=0;$p<3;$p++) {
                        $uploaded_media = Twitter::uploadMedia(['media' =>file_get_contents(public_path().'/images/social_media/'.$t['post_images'][$p]['image_name'] ) ]);  
                        if(!empty($uploaded_media)) {
                            $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                        }
                      }
                    } else {
                      foreach($t['post_images'] as $img) {
                        $uploaded_media = Twitter::uploadMedia(['media' =>file_get_contents(public_path().'/images/social_media/'.$img['image_name'] ) ]);  
                        if(!empty($uploaded_media)) {
                            $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                        }
                      }     
                    } 
                    try {
                      Twitter::reconfig(['consumer_key' =>$twDetails->twitter_consumer_key, 
                                        'consumer_secret' => $twDetails->twitter_consumer_secret, 
                                        'token' => $twDetails->twitter_access_token,  
                                        'secret' => $twDetails->twitter_access_token_secret 
                                      ]);   
                      $twitter = Twitter::postTweet($newTwitte);
                      DB::table('posts')->where('id',$t['id'])->update(['is_posted_on_tw'=>1]);
                      // $this->info('Posted on  '.$twDetails->twitter_account.'\'s twitter account');  
                      return redirect('/posts/')->with('success','Posted on  '.$twDetails->twitter_account.'\'s twitter account'); 
                    } catch(Exception $e) {
                      // $this->info('Sorry, something went wrong,error: '.$e->getMessage()); 
                        return redirect('/posts/')->with('error','Sorry, something went wrong,error: '.$e->getMessage());     
                    }       
                } 
            }         
        } else {     
            // $this->info('Sorry, No posts found to post to twitter'); 
            return redirect()->back()->with('error','Sorry no posts found to post to twitter.');
        }
    }     
    public function showEdit($id) {
        $post=Post::whereId($id)->with('post_images')->first()->toArray();
        // $post=Post::where('id',$id)->with('post_images')->first()->toArray();   
          // dd($post); 
        if($post) {
            if($post['scheduled_at']) {
                $post['scheduled_at']=date('d-m-Y H:i',strtotime($post['scheduled_at']));   
            }
        }
      
        $fbUsers=FacebookAccount::pluck('fb_user_name','id');
        $twUsers=TwitterAccount::pluck('twitter_account','id');
        return view('posts.edit',compact('post','fbUsers','twUsers'));    
    }
    public function updatePost(Request $req,$id) {
        $this->validate($req,[
            'post_title'=>'required|max:300',
            'post_content'=>'required',
            'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048'  // size in KB  
        ]); 
        $post=Post::whereId($id)->firstOrFail();
        $imgName='';
        $IMGNAMEARR=array();  
        if(isset($req->post_images)) {
            foreach($req->file('post_images') as $f ) {
                if($f->getClientSize()>0) {
                    $path=pathinfo($f->getClientOriginalName());
                    $imgName=uniqid().'.'.$path['extension'];
                    $f->move(public_path().'/images/social_media/',$imgName);
                    $IMGNAMEARR[]=$imgName;
                }
            }
        }
        $post->title=$req->get('post_title');
        $post->content=$req->get('post_content');
        if($req->get('fb_user')) {
            $post->facebook_account_id=$req->get('fb_user');
            $post->fb_post_type=$req->get('fb_post_type');
        }
        if($req->get('tw_user')) {
            $post->twitter_account_id=$req->get('tw_user'); 
        }   
        // $post->user_id=Auth::user()->id;  
        if($req->get('scheduled_at') !='') { 
            $post->scheduled_at=date('Y-m-d H:i',strtotime($req->get('scheduled_at')));    
        }   
        if($post->save()) {
            if(isset($req->post_images)) {
                if(!empty($IMGNAMEARR)) {
                    foreach($IMGNAMEARR as $f) {
                        $postImage=new PostImage();
                        $postImage->post_id=$post->id;
                        $postImage->image_name=$f;
                        $postImage->save();
                    }
                }  
            }
            return redirect('/posts/')->with('success','Post updated successfully.'); 
        } else {
            return redirect()->back()->with('error','Post could not be updated,please try again.');
        }
    }    
    public function delete($id) {
        // $post=Post::whereId($id)->firstOrFail();
        $post=Post::where('id',$id)->with('post_images')->first()->toArray();   
        $imgToDelete=$post['post_images'];
        if($post) {  
            if(Post::destroy($id)) {
                foreach($imgToDelete as $img) {
                    $FILETODELETE=public_path().'/images/social_media/'.$img['image_name'];
                    if($FILETODELETE) {
                        \File::delete($FILETODELETE);   
                    }     
                }   
                return redirect('/posts/')->with('success','Post deleted successfully.');
            } else {
                return redirect()->back()->with('error','Sorry,post could not be deleted,please try again.');
            }
        }
    }     
    public function approvePost($id) {
        $post=Post::whereId($id)->firstOrFail();
        if($post) {
            $post->status=1;
            if($post->save()) {
                if( !$post->scheduled_at ) {
                    if($post->post_type==1) {
                        $this->facebookPost($post->id);
                        return redirect('/posts')->with('success','Post approved successfully and posted to Facebook.');
                    } else if($post->post_type==2){
                        $this->twitterPost($post->id);
                        return redirect('/posts')->with('success','Post approved successfully and posted to Twitter.');
                    } else if($post->post_type==3) {
                        $this->facebookPost($post->id);
                        $this->twitterPost($post->id);
                        return redirect('/posts')->with('success','Post approved successfully and posted to Facebook & Twitter.');  
                    } 
                }  
                return redirect('/posts/')->with('success','Post approved successfully.');
            }
            return redirect()->back()->with('error','Sorry, post could not be approved,please try again.');
        }
    }        
    public function showInstaGram() {
        return view('posts.post_to_insta');
    } 
    public function postInstaGram(Request $req) {
        dd($req);
    }
    public function showFacebookPage() {
         return view('posts.all_fb_acc');
    }     
    public function getFacebookAccounts() {
        $facebooks = FacebookAccount::select(['id', 'fb_user_name','fb_app_id','created_at'])->get();    
        $str='';
        return Datatables::of($facebooks)
            ->addColumn('action', function ($fb) { 
                $str ='&nbsp;<a href="/posts/view_fb_acc/'.$fb->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i></a>'; 
                $str .='&nbsp;<a href="/posts/edit_fb_acc/'.$fb->id.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i></a>'; 
                $str .='&nbsp;<a href="/posts/delete_fb_acc/'.$fb->id.'" postid="'.$fb->id.'"  id="postDeleteBTN" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';      
                return $str;   
            })
            ->editColumn('created_at',function($date) {
                return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
            })   
            ->escapeColumns([]) 
            ->make(true);    
    }
    public function viewFacebookAccount($id) {   
        $fb=FacebookAccount::whereId($id)->firstOrFail();
        return view('posts.view_fb_acc',compact('fb')); 
    }
    public function editFacebookAccount($id) {
        $fb=FacebookAccount::whereId($id)->firstOrFail();
        return view('posts.edit_fb_acc',compact('fb')); 
    }
    public function updateFacebookAccount(Request $req,$id) {
        $this->validate($req,[  
              'fb_user_name'=>'required',
              'fb_app_id'=>'required',
              'fb_app_secret'=>'required',
              'fb_user_access_token'=>'required',
              'fb_page_id'=>'required',
              'fb_page_name'=>'required'
        ]);    
        $fbAcc=FacebookAccount::whereId($id)->firstOrFail();
        $fbAcc->fb_user_name=$req->get('fb_user_name');
        $fbAcc->fb_app_id=$req->get('fb_app_id');   
        $fbAcc->fb_app_secret=$req->get('fb_app_secret');     
        $fbAcc->fb_user_access_token=$req->get('fb_user_access_token');
        $fbAcc->fb_page_id=$req->get('fb_page_id');    
        $fbAcc->fb_page_name=$req->get('fb_page_name');     
        if($fbAcc->save()) { 
           return redirect('/posts/all_fb_acc')->with('success','Account updated successfully.');
        } else {
          return redirect()->back()->with('error','Sorry,Account could not be updated,please try again.');
        }   
    }
    public function deleteFacebookAccount($id) {
        $fb=FacebookAccount::whereId($id)->firstOrFail();
        if($fb) {
            if($fb->delete()) { 
                return redirect('/posts/all_fb_acc')->with('success','Account deleted successfully.');
            } else {
                return redirect()->back()->with('error','Sorry,account could not be deleted,please try again.');
            }
        }
    }
    public function showTwitterPage() {  
        return view('posts.all_twitter_acc');
    }
    public function getTwitterAccounts() { 
        $twitters = TwitterAccount::select(['id', 'twitter_account','twitter_consumer_key','created_at'])->get();    
        $str='';
        return Datatables::of($twitters) 
            ->addColumn('action', function ($t) { 
                $str ='&nbsp;<a href="/posts/view_twitter_acc/'.$t->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i></a>'; 
                $str .='&nbsp;<a href="/posts/edit_twitter_acc/'.$t->id.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i></a>'; 
                $str .='&nbsp;<a href="/posts/delete_twitter_acc/'.$t->id.'" postid="'.$t->id.'"  id="postDeleteBTN" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';      
                return $str;   
            })
            ->editColumn('created_at',function($date) {
                return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
            })   
            ->escapeColumns([]) 
            ->make(true);   
    }
    public function viewTwitterAccount($id) {
        $tw=TwitterAccount::whereId($id)->firstOrFail();
        return view('posts.view_twitter_acc',compact('tw'));  
    }
    public function editTwitterAccount($id) {
        $twitter=TwitterAccount::whereId($id)->firstOrFail();
        return view('posts.edit_twitter_acc',compact('twitter')); 
    }
    public function updateTwitterAccount(Request $req,$id) {
       $this->validate($req,[
             'twitter_account'=>'required',
             'twitter_consumer_key'=>'required',
             'twitter_consumer_secret'=>'required',
             'twitter_access_token'=>'required',
             'twitter_access_token_secret'=>'required'
       ]); 
       $twitterAcc=TwitterAccount::whereId($id)->firstOrFail();
       $twitterAcc->twitter_account=$req->get('twitter_account');
       $twitterAcc->twitter_consumer_key=$req->get('twitter_consumer_key');
       $twitterAcc->twitter_consumer_secret=$req->get('twitter_consumer_secret');
       $twitterAcc->twitter_access_token=$req->get('twitter_access_token');
       $twitterAcc->twitter_access_token_secret=$req->get('twitter_access_token_secret');
       if($twitterAcc->save()) {
             return redirect('/posts/all_twitter_acc')->with('success','Account updated successfully.');
       } else {
         return redirect()->back()->with('error','Sorry,Account could not be updated,please try again.');
       } 
    }
    public function deleteTwitterAccount($id){
        $tw=TwitterAccount::whereId($id)->firstOrFail();
        if($tw) {
            if($tw->delete()) { 
                return redirect('/posts/all_twitter_acc')->with('success','Account deleted successfully.');
            } else {  
                return redirect()->back()->with('error','Sorry,account could not be deleted,please try again.');
            }
        }
    }
    public function showUserPost() {    
        return view('posts.user_post');       
    }
    public function showAddUserPost() {
        $userAccess=UserAccess::where('user_id',Auth::user()->id)->first();
        $twIDS=$fbIDS=$fbUsers=$twUsers=array(); 
        if($userAccess) {
            if($userAccess->post_type==1) {
                $userAccess->post_type_id='Facebook';    
            } else if($userAccess->post_type==2) {
                $userAccess->post_type_id='Twitter';
            } else if($userAccess->post_type==3) {
                $userAccess->post_type_id='Facebook/Twitter Both';
            }
            if($userAccess->fb_post_type==1) {
                $userAccess->fb_post_type_id='Post To FB User Timeline';
            } else {
                $userAccess->fb_post_type_id='Post To FB Page';
            }
            if($userAccess->facebook_account_id) {
                $fbIDS=explode(',',$userAccess->facebook_account_id);    
            }
            if($userAccess->twitter_account_id) {
                $twIDS=explode(',',$userAccess->twitter_account_id);    
            }
            if($fbIDS) {
                $fbUsers=FacebookAccount::find($fbIDS);        
            }
            if($twIDS) {
                $twUsers=TwitterAccount::find($twIDS);  
            } 
        } else {
            return redirect()->back()->with('error','Sorry, you are not authorized to make a post.');
        }  
        return view('posts.add_user_post',compact('fbUsers','twUsers','userAccess','hasUser'));     
    }
    public function getUserPosts() { 
        $posts = Post::select(['id', 'title','status','is_posted_on_fb','is_posted_on_tw','created_at'])->where('user_id',Auth::user()->id)->get();
        $str='';
        return Datatables::of($posts)
            ->addColumn('action', function ($post) { 
                $str='&nbsp;<a href="/posts/user_view/'.$post->id.'"class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i></a>'; 
                $str.='&nbsp;<a href="/posts/user_edit/'.$post->id.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i></a>';
                $str.='&nbsp;<a href="javascript:;" onclick="confirmDel('.$post->id.')" postid="'.$post->id.'"  id="postDeleteBTN" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';       
                return $str;  
            })
            ->editColumn('title',function($title) {
                return str_limit($title->title, $limit = 100, $end = '...');
            }) 
            ->editColumn('created_at',function($date) {
                return date('d-m-Y H:i:s A',strtotime($date->created_at)); 
            }) 
            ->editColumn('status',function($status) {
                if($status->status) {
                    return "<span class='btn btn-success'> Approved </span>";
                } else {
                    return "<span class='btn btn-danger'> Not-Approved </span>";
                }
            })
            ->editColumn('is_posted_on_fb',function($fb) {
                if($fb->is_posted_on_fb) {
                    return "<span class='btn btn-success'> Yes </span>";
                } else {
                    return "<span class='btn btn-danger'> No </span>";
                }
            })
            ->editColumn('is_posted_on_tw',function($tw) {
                if($tw->is_posted_on_tw) {
                    return "<span class='btn btn-success'> Yes </span>";
                } else {
                    return "<span class='btn btn-danger'> No </span>"; 
                } 
            }) 
            ->escapeColumns([]) 
            ->make(true); 
    }    
    public function saveUserPost(Request $req) {
        $this->validate($req,[
            'post_title'=>'required|max:300', 
            'post_content'=>'required', 
            'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048' 
        ]); 
        $IMGNAMEARR=array(); 
        if(count($req->post_images) <=3 ) {
            if(isset($req->post_images)) {
                foreach($req->file('post_images') as $f ) {
                    if($f->getClientSize()>0) {
                        $path=pathinfo($f->getClientOriginalName());
                        $imgName=uniqid().'.'.$path['extension'];
                        $f->move(public_path().'/images/social_media/',$imgName);
                        $IMGNAMEARR[]=$imgName;
                    }
                }
            }
            $post=new Post();
            $post->title=$req->get('post_title');
            $post->content=$req->get('post_content');
            $post->facebook_account_id=$req->get('fb_user');
            $post->twitter_account_id=$req->get('tw_user'); 
            $post->fb_post_type=$req->get('fb_post_type');
            $post->post_type=$req->get('post_type');
            $post->user_id=Auth::user()->id;  
            if($req->get('scheduled_at') !='') { 
                $post->scheduled_at=date('Y-m-d H:i',strtotime($req->get('scheduled_at')));   
            }  
            if($post->save()) {
                if(isset($req->post_images)) {
                    if(!empty($IMGNAMEARR)) {
                        foreach($IMGNAMEARR as $f) {
                            $postImage=new PostImage();
                            $postImage->post_id=$post->id;
                            $postImage->image_name=$f;
                            $postImage->save();
                        }   
                    }
                }       
                return redirect('/posts/user_posts')->with('success','Post added successfully.');   
            } else {
                return redirect()->back()->with('error','Post could not be added,please try again.');
            }
        } else {
            return redirect()->back()->with('error','Sorry, max. 3 images are allowed to upload.');
        } 
    }
    public function showUserEdit($id) {
        $post=Post::where('id',$id)->with('post_images')->first()->toArray();
        $twIDS=$fbIDS=$fbUsers=$twUsers=array();  
        if($post['scheduled_at']) {
            $post['scheduled_at']=date('d-m-Y H:i',strtotime($post['scheduled_at']));   
        } 
        $userAccess=UserAccess::where('user_id',Auth::user()->id)->firstOrFail(); 
        if($userAccess) { 
            if($userAccess->post_type==1) {
                $userAccess->post_type_id='Facebook';    
            } else if($userAccess->post_type==2) {
                $userAccess->post_type_id='Twitter';
            } else if($userAccess->post_type==3) {
                $userAccess->post_type_id='Facebook/Twitter Both';
            }
            if($userAccess->fb_post_type==1) {
                $userAccess->fb_post_type_id='Post To FB User Timeline';
            } else {
                $userAccess->fb_post_type_id='Post To FB Page';
            }
            if($userAccess->facebook_account_id) {
                $fbIDS=explode(',',$userAccess->facebook_account_id);    
            }
            if($userAccess->twitter_account_id) {
                $twIDS=explode(',',$userAccess->twitter_account_id);    
            }
            if($fbIDS) {
                $fbUsers=FacebookAccount::find($fbIDS);        
            }
            if($twIDS) {
                $twUsers=TwitterAccount::find($twIDS);  
            } 
        }  
        return view('posts.user_edit',compact('post','fbUsers','twUsers','userAccess'));   
    }
    public function updateUserPost(Request $req,$id) {
        $this->validate($req,[
            'post_title'=>'required|max:200',
            'post_content'=>'required',
            'post_images.*'=>'required|mimes:jpeg,png,jpg,gif|max:2048'  // size in KB  
        ]); 
        $post=Post::whereId($id)->firstOrFail();
        $imgName=''; 
        $IMGNAMEARR=array();  
        if(isset($req->post_images)) {
            foreach($req->file('post_images') as $f ) {
                if($f->getClientSize()>0) {
                    $path=pathinfo($f->getClientOriginalName());
                    $imgName=uniqid().'.'.$path['extension'];
                    $f->move(public_path().'/images/social_media/',$imgName);
                    $IMGNAMEARR[]=$imgName; 
                }
            }
        }
        $post->title=$req->get('post_title');
        $post->content=$req->get('post_content');
        if($req->get('fb_user')) {
            $post->facebook_account_id=$req->get('fb_user');
        }
        if($req->get('tw_user')) {
            $post->twitter_account_id=$req->get('tw_user');
        }
        if($req->get('scheduled_at') !='') { 
            $post->scheduled_at=date('Y-m-d H:i',strtotime($req->get('scheduled_at')));   
        }   
        if($post->save()) {
            if(isset($req->post_images)) {
                if(!empty($IMGNAMEARR)) {
                    foreach($IMGNAMEARR as $f) {
                        $postImage=new PostImage();
                        $postImage->post_id=$post->id;
                        $postImage->image_name=$f;
                        $postImage->save();
                    }
                }  
            }    
            return redirect('/posts/user_posts')->with('success','Post updated successfully.');  
        } else {
            return redirect()->back()->with('error','Post could not be updated,please try again.');
        }
    } 
    public function deleteUserPost($id) {
        $post=Post::where('id',$id)->with('post_images')->first()->toArray();   
        $imgToDelete=$post['post_images'];
        if($post) {  
            if(Post::destroy($id)) {
                foreach($imgToDelete as $img) {
                    $FILETODELETE=public_path().'/images/social_media/'.$img['image_name'];
                    if($FILETODELETE) {
                        \File::delete($FILETODELETE);   
                    }     
                }   
                return redirect('/posts/user_posts')->with('success','Post deleted successfully.');
            } else {
                return redirect()->back()->with('error','Sorry,post could not be deleted,please try again.');
            }
        }
    } 
    public function deleteImages($id) {
        $res=array();
        if($id) {
            if(PostImage::destroy($id)) {
                $res['success']=true;
                // return redirect('/posts/edit/49')->with('success','Image deleted.');
            } else {
                $res['success']=false; 
                // return redirect()->back()->with('error','Image could not be deleted,please try again!.');
            }
            echo json_encode($res);
        }
    }


}
