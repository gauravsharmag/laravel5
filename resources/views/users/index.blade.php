@extends('layouts.back')
@section('title','View All Users')
@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> Users </h2>
		</div>
		<table class="table" id="usersTable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>     
					<th>Role</th>
					<th>Email</th>
					<th>Registered On</th>
					<th>Actions</th>
				</tr>
			</thead> 
		</table>
	</div>
		<script>
			$(function() {
			    $('#usersTable').DataTable({
			        processing: true,
			        serverSide: true,
			        ajax: "{{ route('datatable.getUsers') }}",    
			        columns: [
			            { data: 'id', name: 'id' },
			            { data: 'name', name: 'name' },
			            { data: 'role_id', name: 'role_id' }, 
			            { data: 'email', name: 'email' }, 
			            { data: 'created_at', name: 'created_at'},    
			            { data: 'action', name: 'action'}    
			        ]
			    });
			});
			function confirmDel(id) {
				if(window.confirm("Are you sure?")) {  
					window.location.href='/users/delete/'+id;
				} else { 
					return false;
				} 
			} 
			function confirmSwitch(id) {
				if(window.confirm("Are you sure?")) {  
					window.location.href='/users/switch/'+id;
				} else {  
					return false;
				} 
			}
		</script>
@endsection