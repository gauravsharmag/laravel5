@extends('layouts.homelayout')
@section('title',__('| Sign In'))
@section('content')
<div class="row"> 
	<div class="col-md-4">  </div>
	<div class="col-md-4">
		<div class="well well bs-component">    
			{!! Form::open(array('url'=>'/signin')) !!}
				<div class="form-group">
					{!! Form::label('email',__('Email')) !!}
					{!! Form::text('email','',array('class'=>'form-control')) !!}
					<span class="text-danger"> {{$errors->first('email')}} </span>
				</div>
				<div class="form-group">
					{!! Form::label('password',__('Password')) !!}
					{!! Form::password('password',array('class'=>'form-control')) !!}
					<span class="text-danger"> {{$errors->first('password')}}</span>
				</div>
				{!! Form::submit(__('SignIn'),array('class'=>'btn btn-default')) !!}
			{!! Form::close() !!}
		</div>
	</div>
	<div class="col-md-4">  </div>
</div>  
<div class="row"> 
	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
			<h3> <?php echo __('OR Signin with Social Media'); ?> </h3>     
			<a style="font-size: 22px;" href="{{ url('/auth/linkedin') }}" class="btn btn-linkedin">
				<i class="fa fa-linkedin"></i>
			</a> 
			<a style="font-size: 22px;" href="{{ url('/auth/facebook') }}" class="btn btn-facebook">
				<i class="fa fa-facebook"></i>
			</a>
			<a style="font-size: 22px;" href="{{ url('/auth/google') }}" class="btn btn-google">
				<i class="fa fa-google"></i>
			</a>
			<a style="font-size: 22px;" href="{{ url('/auth/twitter') }}" class="btn btn-twitter">
				<i class="fa fa-twitter"></i>
			</a>
			<a style="font-size: 22px;" href="{{ url('/auth/github') }}" class="btn btn-github">
				<i class="fa fa-github"></i>
			</a> 
		</div>
	</div>
</div>
@endsection