@extends('layouts.back')
@section('title','Add User')
@section('content') 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.jquery.min.js"></script> 
<div class="row"> 
	<div class="col-md-2">  </div>
	<div class="col-md-8">
		{!! Form::open() !!}
			<div class="form-group">
				{!!  Form::label('name', 'Name') !!}
				{!!  Form::text('name','',array('class'=>'form-control')) !!}
				<span class="text-danger"> {{ $errors->first('name')  }}</span>
			</div>
			<div class="form-group">
				{!!  Form::label('email', 'Email') !!}   
				{!!  Form::text('email','',array('class'=>'form-control')); !!}
				<span class="text-danger"> {{$errors->first('email')}}</span>
			</div>
			<div class="form-group">
				{!!  Form::label('password', 'Password') !!}
				{!!  Form::password('password',array('class'=>'form-control')) !!}
				<span class="text-danger">{{$errors->first('password')}} </span>
			</div>
			<div class="form-group">
				{!!  Form::label('role_id', 'Role') !!}
				{!!  Form::select('role_id',array('2'=>'User','1'=>'Admin'),null,['class'=>'form-control'] ) !!}	
			</div>
			<div class="form-group" id="postTypeDiv">
			    <label>Select Post Type:</label>   
			    <select name="post_type" id="postType" class="form-control">
			    	<option value="">Select</option>
			    	<option value="1" {{ old('post_type')=='1' ? 'selected="selected"' : '' }}>Facebook</option>
			    	<option value="2" {{ old('post_type')=='2' ? 'selected="selected"' : '' }}>Twitter</option>
			    	<option value="3" {{ old('post_type')=='3' ? 'selected="selected"' : '' }}>Both</option> 
			    </select>
			    <span class="text-danger">{{$errors->first('post_type')}} </span>
			</div> 

			<div class="form-group" id="fb_user" style="display: none;" >     
			    <label>Select Facebook Users:</label>
			    {!! Form::select('fb_user',$fbUsers->toArray(), null,['multiple'=>'true', 'class'=>'form-control','id'=>'fbUserID','selected'=>" old('fb_user')"] ) !!}
			    <input type="hidden" name="fb_user_ids" id="fbUserIDS">    
			    <span class="text-danger">{{$errors->first('fb_user')}} </span>   
			</div> 

			<div class="form-group" id="fbPostType" style="display: none;">
			    <label>Select FB Post Type:</label>   
			    <select name="fb_post_type" id="FBPOSTTYPE" class="form-control" selected="{{old('fb_post_type')}}">
			    	<option value="1" {{ old('fb_post_type')=='1' ? 'selected="selected"' : '' }}>Post To FB User Timeline</option>
			    	<option value="2" {{ old('fb_post_type')=='2' ? 'selected="selected"' : '' }}>Post To FB Page</option>   
			    </select> 
			</div>

			<div class="form-group" id="tw_user" style="display: none;">  
			    <label>Select Twitter Users:</label>   
			    {!! Form::select('tw_user',$twUsers->toArray(), null,['multiple'=>'true','class'=>'form-control','id'=>'twUserID','selected'=>"old('tw_user')" ] ) !!} 
			    <input type="hidden" name="tw_user_ids" id="twUserIDS">     
			    <span class="text-danger">{{$errors->first('tw_user')}} </span>          
			</div>

			{!! Form::submit('Submit',array('class'=>'btn btn-default'),'',['class'=>'form-control']); !!} 
		{!! Form::close() !!}  
	</div>
	<div class="col-md-2">  </div>
</div>
<script type="text/javascript">
	$("#fbUserID").chosen({ 
	    // disable_search_threshold: 10,
	    no_results_text: "Oops, No facebook user found!",
	    placeholder_text_multiple:"Select Facebook Users",
	    width: "100%"
	});    
	$("#twUserID").chosen({ 
	    // disable_search_threshold: 10,
	    no_results_text: "Oops, No twitter user found!",
	    placeholder_text_multiple:"Select Twitter Users",
	    width: "100%"
	});
	$("#fbUserID").chosen().change(function() {
		$('#fbUserIDS').val($(this).val()); 
	}); 
	$("#twUserID").chosen().change(function() {
		$('#twUserIDS').val($(this).val()); 
	}); 

	postType($('#postType').val());
	function postType(id) {
		if(id) {
			if(id==1) {
				$('#fb_user').show();
				$('#fbPostType').show();
				$('#tw_user').hide();
			} else if(id==2) {
				$('#fb_user').hide();
				$('#fbPostType').hide();
				$('#tw_user').show();
			} else if(id==3) {
				$('#fb_user').show();
				$('#fbPostType').show();
				$('#tw_user').show();
			} else {
				$('#fb_user').hide();
				$('#fbPostType').hide();
				$('#tw_user').hide();
			}
		} 
	}      
	$('#postType').change(function() {
		postType($(this).val());
	});
</script>
@endsection