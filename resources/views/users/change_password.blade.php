@extends('layouts.back') 
@section('title','Change Password')
@section('content')
	
	<div class="well well bs-component">   
		<form class="form-horizontal" action="/users/change_password" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<fieldset>
				<legend>Change Password</legend>
				
				<div class="form-group">
					<label for="name" class="col-lg-2 control-label">Password</label>
					<div class="col-lg-10">
						<input type="password" class="form-control" id="password" name="password" >
						<span class="text-danger"> {{$errors->first('password')}} </span>         
					</div>
				</div>
				<div class="form-group">
					<label for="content" class="col-lg-2 control-label">Confirm Password</label>
					<div class="col-lg-10">
						<input type="password" class="form-control" id="confirm_password" name="confirm_password" >
						<span class="text-danger"> {{$errors->first('confirm_password')}} </span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>


@endsection