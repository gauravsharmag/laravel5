@extends('layouts.homelayout')
@section('title','|Sign Up')
@section('content') 
<div class="row"> 
	<div class="col-md-2">  </div>
	<div class="col-md-8">
		{!! Form::open(array('url' => '/signup')) !!}
			<div class="form-group">
				{!!  Form::label('name', 'Name') !!}
				{!!  Form::text('name','',array('class'=>'form-control')) !!}
				<span class="text-danger"> {{ $errors->first('name')  }}</span>
			</div>
			<div class="form-group">
				{!!  Form::label('email', 'Email') !!} 
				{!!  Form::text('email','',array('class'=>'form-control')); !!}
				<span class="text-danger"> {{$errors->first('email')}}</span>
			</div>
			<div class="form-group">
				{!!  Form::label('password', 'Password') !!}
				{!!  Form::password('password',array('class'=>'form-control')) !!}
				<span class="text-danger">{{$errors->first('password')}} </span>
			</div>
			{!! Form::submit('Submit',array('class'=>'btn btn-default')); !!} 
		{!! Form::close() !!}  
	</div>
	<div class="col-md-2">  </div>
</div>
@endsection