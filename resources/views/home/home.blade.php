@extends('layouts.homelayout') 
@section('content')
<style type="text/css">
	#backImg {
			background: url(/images/home_bg.jpg) no-repeat center center fixed;
		    -webkit-background-size: cover;
		    -moz-background-size: cover;
		    -o-background-size: cover;
		    background-size: cover;	
		    height: 800px;
	}
	#homeTagLine {
		color: white;
		text-align: center;
	}
</style>
   
<div id="backImg"> 
	<h2 id="homeTagLine"> <?php echo __('Language'); ?>:{{ __(App::getLocale()) }}</h2>    
	<h2 id="homeTagLine"><?php echo __('Laravel') ?> {{ App::VERSION() }}</h2> 
</div> 

@endsection
