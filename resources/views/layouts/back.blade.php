<!doctype html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>@yield('title')</title> 
 <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
 {!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') !!} 
 {!! Html::style('css/app.css') !!}
 {!! Html::style('css/custom.css') !!} 
 {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') !!}
 {!! Html::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') !!}
</head>
<body>
 <div class="container">
   @include('elements.front_header')   
   @if(session('success'))
   	<div class="alert alert-success"> 
   	{{ session('success') }}
   	</div>
   @endif
   @if(session('error'))
   	<div class="alert alert-danger">  
   	{{ session('error') }}
   	</div>
   @endif 
   <div class="row"> 
      @if(Auth::check())
       @if(Auth::user()->role_id==1) 
      <div class="col-md-3"> 
         @include('elements.admin_sidebar')
      </div>
      @else
        <div class="col-md-3"> 
           @include('elements.user_sidebar') 
        </div>
      @endif
      @endif
      <div class="col-md-9">
          @yield('content')
      </div>

   </div>
   
    <footer class="row">
     <div class="col-md-3"> </div>
      <div class="col-md-6"> 
             @include('elements.front_footer')
      </div>
      <div class="col-md-3"> </div>
      
    </footer>
   </div>
</body> 
</html>