@extends('layouts.homelayout') 
@section('content')
<h3>Contact Us</h3>
<form method="POST" action="/contact" accept-charset="UTF-8" class="form">
	<label for="name">Your Name</label>
	<input class="form-control" placeholder="Your name"
	name="name" type="text" required="required">

	<label for="email">Your E-mail Address</label>
	<input class="form-control" placeholder="Your e-mail address"
	name="email" type="text" required="required">

	<label for="message">Your Message</label>
	<textarea class="form-control" name="message"
	required="required"></textarea>

	<input class="btn btn-primary" type="submit" value="Contact Us!">
</form>
@endsection