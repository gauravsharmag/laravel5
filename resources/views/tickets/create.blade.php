@extends('layouts.homelayout')
@section('title',__('Contact'))
@section('content')



<div class="container col-md-8 col-md-offset-2">
	<div class="well well bs-component">
		<form class="form-horizontal" method="post" action="/contact"> 
		<!-- {!! Form::open(array('url'=>'/signin')) !!} -->
		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<fieldset>
				<legend><?php echo __('Submit a new ticket'); ?></legend>
				<div class="form-group">
					<label for="title" class="col-lg-2 control-label"><?php echo __('Title'); ?>
					</label>
						<div class="col-lg-10">
							<input type="text" name="title" class="form-control" id="title"  value="{{ old('title') }}"> 
							<span class="text-danger"> {{$errors->first('title')}} </span>
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-lg-2 control-label"><?php echo __('Content'); ?></label>
							<div class="col-lg-10">
								<textarea class="form-control" name="content" rows="3" id="content">
									{{ old('content') }}  
								</textarea>
								<span class="text-danger"> {{$errors->first('content')}} </span> 
								<span class="help-block"><?php echo __('Feel free to ask us any question'); ?></span>
								</div>
							</div> 
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary"><?php echo __('Submit'); ?></button>
								</div>
							</div>
						</fieldset>
				</form>
				<!-- {!! Form::close() !!} -->
				</div>
			</div>

@endsection