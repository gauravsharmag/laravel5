<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
     {!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') !!} 
	 {!! Html::style('css/app.css') !!} 
	 {!! Html::style('css/custom.css') !!} 
	 {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') !!}
	 {!! Html::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') !!}
  </head>
  <body> 
   <div class="row"> 
   		<span> <h2 style="text-align: center;">Laravel First App</h2> </span>
   		<div class="col-md-3"> </div>
   		<div class="col-md-6"> 
   			<div class="well well bs-component">
   				<div class="content">
   				<div style="float:right;"> 
   					<img src="{{asset('images/hotel_56d569512fdd7.jpeg')}}" />  
   				</div>	
   				
   					<!-- <img src="/images/home_bg.jpg" />  -->
   					<h2 class="header">{!! $ticket->title !!}</h2>
   					<p> <strong>Status</strong>: {!! $ticket->status ? 'Pending' : 'Answered' !!}</p>
   						<p> {!! $ticket->content !!} </p>
   				</div> 
   			</div>
   		</div>
   		<div class="col-md-3"> </div>

   </div>

  </body>
  </html>
