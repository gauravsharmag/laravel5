@extends('layouts.back')
@section('title','View Ticket')
@section('content') 
	  

	<div class="well well bs-component">
		<div class="content">
			<h2 class="header">{!! $ticket->title !!}</h2>
			<p> <strong>Status</strong>: {!! $ticket->status ? 'Pending' : 'Answered' !!}</p>
				<p> {!! $ticket->content !!} </p>
		</div> 
		<a href="/tickets/pdf/{{$ticket->slug}}"  class="btn btn-warning"> Download PDF </a>    
		<a href="/tickets/view/pdf/{{$ticket->slug}}"  class="btn btn-success" target="_blank"> View PDF </a>  

		<a href="/tickets/edit/{{$ticket->slug}}" class="btn btn-info">Edit</a>
		<form method="post" action="{!! action('TicketsController@delete', $ticket->slug) !!}" class="pull-left">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<div class="form-group">
				<div> 
					<button type="submit" class="btn btn-danger">Delete</button>&nbsp;
				</div>
			</div>
		</form>
	</div>



@endsection