@extends('layouts.back')
@section('title','View All Tickets')
@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>	

	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> Tickets </h2>
		</div>
		@if ($tickets->isEmpty())
		<p> There is no ticket.</p>
		@else
		<table class="table" id="ticketsTable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Created On</th>
					<th>Status</th>
					<th>Action</th> 
				</tr>
			</thead>
		</table>
			@endif
		</div>  


<script>
	$(function() {
	    $('#ticketsTable').DataTable({ 
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('datatable.gettickets') }}",    
	        columns: [
	            { data: 'id', name: 'id' },
	            { data: 'title', name: 'title' },
	            { data: 'created_at', name: 'created_at' }, 
	            { data: 'status', name: 'status' }, 
	            { data: 'action', name: 'action'}   
	        ]
	    });
	});
	function confirmDel(id) {
		if(window.confirm('Are you sure?')) {
			window.location.href='/tickets/delete/'+id; 
		} else {
			return false;
		} 
	}
</script>
@endsection

