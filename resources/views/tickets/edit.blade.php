@extends('layouts.back') 
@section('title','Edit Ticket')
@section('content')
	
	<div class="well well bs-component">
		<form class="form-horizontal" method="post">
			@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
			@endif
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<fieldset>
				<legend>Edit ticket</legend>
				<div class="form-group">
					<label for="title" class="col-lg-2 control-label">Title</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" id="title" name="title" value="{!! $ticket->title !!}">
						<span class="text-danger"> {{$errors->first('title')}} </span>
					</div>
				</div>
				<div class="form-group">
					<label for="content" class="col-lg-2 control-label">Content</label>
					<div class="col-lg-10">
						<textarea class="form-control" rows="3" id="content" name="content">{!! $ticket->content !!}</textarea>
						<span class="text-danger"> {{$errors->first('content')}} </span>
					</div>
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" name="status" {!! $ticket->status ? "checked" : "" !!} > Close this ticket?
					</label>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>


@endsection