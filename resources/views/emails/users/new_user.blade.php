@component('mail::message')
<!-- <img src="https://homestay.netgen.in/img/static/shimlahp.jpg"> -->
Hi, {{ $user->name }} <br> 
Thanks for regestering with us.

@component('mail::button', ['url' => 'http://laravel.netgen.in'])
{{__('Button Text') }}
@endcomponent

{{ __('Thanks') }},<br>
{{ config('app.name') }}
@endcomponent
