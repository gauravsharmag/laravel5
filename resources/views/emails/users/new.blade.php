@component('mail::message')
# {{__('Introduction') }}

<div>
	firstKey: {{ $firstKey }} <br>
	secondKey: {{ $secondKey }} 
</div>
@component('mail::panel')
	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
@endcomponent

@component('mail::table')
| {{__('User') }}       | {{ __('Email') }}| {{ __('Role') }}  |
| ------------- |:-------------:| --------:|
| {{ $firstColumn }}|{{ $secondColumn }}| {{ $thirdColumn }}|


@endcomponent 

@component('mail::button', ['url' => 'http://laravel.netgen.in'])
{{__('Button Text') }}
@endcomponent

{{ __('Thanks') }},<br>
{{ config('app.name') }}
@endcomponent
