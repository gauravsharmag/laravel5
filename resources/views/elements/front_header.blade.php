<div class="row"> 
  <div class="col-md-12">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="/"> 
              <?php echo __("Laravel First App") ?>
            </a>  
          </div>
          <ul class="nav navbar-nav">
            <li class=""><a href="/"><?php echo __('Home'); ?></a></li> 
            <li><a href="/about"><?php echo __('About') ?></a></li> 
            <li><a href="/contact"><?php echo __('Contact') ?></a></li>     
            @auth
                <div class="dropdown" style="position: absolute;margin-left: 238px;top: 13px;">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Hi,{{Auth::user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      @if(Auth::user()->role_id==1)
                        <li><a href="/users/dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;Dashboard</a></li>
                        <li><a href="/users/profile/{{Auth::user()->id }}"> <i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>
                      @else
                      <li><a href="/users/user_dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;Dashboard</a></li>
                      <li><a href="/users/user/{{Auth::user()->id }}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profile</a></li> 
                      @endif
                        <li><a href="/users/change_password"><i class="fa fa-key" aria-hidden="true"></i>&nbsp;Change Password</a></li>
                        <li><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a></li>
                    </ul>
                </div> 
            @else 
              <li><a href="/signin"><?php echo __('SignIn') ?> </a></li>  
              <select name="languageSelect" id="languageSelect" style="margin-top:13px;margin-left:66px;float: left;">
                <option value="en" {{ App::getLocale()=='en' ? 'selected="selected"' : '' }} >
                  <?php echo __('English'); ?>
                </option>
                <option value="hi" {{ App::getLocale()=='hi' ? 'selected="selected"' : '' }}>
                  <?php echo __('Hindi'); ?>
                </option>
              </select>
            @endauth
          </ul>
        </div>
      </nav>
   </div>
</div>
<script type="text/javascript">
    $('#languageSelect').change(function() {
        window.location.href='/lang/'+$(this).val();
    }); 
</script>
