
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   <div id="wrapper" style="height: 500px;">
      <!-- Sidebar -->     
      <div id="sidebar-wrapper">
         <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
            <li class="active">
               <a href="/users/user_dashboard">
                     <span class="fa-stack fa-lg pull-left">
                        <i class="fa fa-dashboard fa-stack-1x "></i>    
                     </span>
                      Dashboard
                  </a>  
            </li>
           <li>
               <a href="javascript:;">
                  <span class="fa-stack fa-lg pull-left">
                    <i class="fa fa-newspaper-o fa-stack-1x "></i>
                     </span>
                     Posts  
                  </a>
                  <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                     <li><a href="/posts/user_posts">View All</a></li>
                     <li><a href="/posts/add_user_post">Add New</a></li>  
                  </ul>
            </li>
            @if(session()->has('back_id')) 
            <li>
               <a href="/users/switch_back/{{ session('back_id') }}"> 
                     <span class="fa-stack fa-lg pull-left">
                        <i class="fa fa-dashboard fa-stack-1x "></i>    
                     </span>
                      Switch Back
                  </a>  
            </li>
            @endif
         
             <!--  <li>
               <a href="/tickets"> 
                     <span class="fa-stack fa-lg pull-left">
                        <i class="fa fa-envelope fa-stack-1x "></i>
                     </span>
                     Contact Messages
                  </a>
            </li> -->
         </ul>
      </div>
      <!-- /#sidebar-wrapper -->
      <!-- Page Content -->
      <div id="page-content-wrapper">
         <div class="container-fluid xyz">
            <div class="row">
              
            </div>
         </div>
      </div>
      <!-- /#page-content-wrapper -->
   </div>
   <!-- /#wrapper -->
   <!-- jQuery -->





<script type="text/javascript">
   
   $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
   });
   $("#menu-toggle-2").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled-2");
      $('#menu ul').hide();
   });

   function initMenu() {
      $('#menu ul').hide();
      $('#menu ul').children('.current').parent().show();
      //$('#menu ul:first').show();
      $('#menu li a').click(
         function() {
            var checkElement = $(this).next();
            if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
               return false;
            }
            if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
               $('#menu ul:visible').slideUp('normal');
               checkElement.slideDown('normal');
               return false;
            }
         }
      );
   }
   $(document).ready(function() {
      initMenu();
   });

</script>