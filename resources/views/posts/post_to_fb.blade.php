@extends('layouts.back')
@section('title','Add Post To Facebook')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content') 

<div class="row"> 
	<div class="col-md-2">  </div>  
	<div class="col-md-8"> 

	<!-- <form class="form-horizontal" method="post" id="uploadForm" action="/posts/post_to_fb" enctype="multipart/form-data"> -->
	<form class="form-horizontal" method="post" id="uploadForm" action="/posts/post_fb" enctype="multipart/form-data">
 
	
		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
		@if(count($errors))
		    <div class="alert alert-danger">
		        <strong>Whoops!</strong> There were some problems with your input.
		        <br/>
		        <ul>  
		            @foreach($errors->all() as $error)
		            <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>  
		@endif
		<fieldset>
			<div class="form-group" id="fbUserAccessToken">
			    <label>Select User:</label>   
			    {!! Form::select('fb_user_access_token',[''=>'Select User']+$fbUserTokens->toArray(), null,['class'=>'form-control','id'=>'UserID'] ) !!} 
			</div>

			<div class="form-group">
			    <label>Select Post Type:</label>   
			    <select name="fb_post_type" id="FBPOSTTYPE" class="form-control">
			    	<option value="1">Post To FB User Timeline</option>
			    	<option value="2">Post To FB Page</option>   
			    </select>
			</div>
			
			<!-- <div class="form-group" id="fbPageID" style="display: none;"> 
			    <label>Select Page:</label>    
			    {!! Form::select('fb_page_id', $fbPageIDs, null,['class'=>'form-control'] ) !!} 
			</div> -->
			<div class="form-group" id="fbPageID" style="display:none;" > 
				 <label>Select Page:</label> 
				 <select name="fb_page_id" id="fbPageIDS" class="form-control">
				 	
				 </select>
			</div>

			<!-- <div class="form-group" id="fbAppID">
			    <label>Select Facebook App ID:</label>   
			    {!! Form::select('fb_app_id', $fbAppIDs, null,['class'=>'form-control'] ) !!} 
			</div>
			<div class="form-group" id="fbAppSecret">
			    <label>Select Facebook App Secret:</label>   
			    {!! Form::select('fb_app_secret', $fbAppSecrets, null,['class'=>'form-control'] ) !!}  
			</div> -->
			
			<!-- <div class="form-group" id="pageIdDiv" style="display: none;"> 
				<label>Enter Facebook Page ID:</label>
				<input type="text" class="form-control" name="fb_pageID" id="fbPageID">	
			</div> -->

			<div class="form-group">
				<label>Add Post Text:</label>
				<input type="text" class="form-control" name="facebook_post" id="facebookPost">
			</div>
			  
			<div class="form-group"> 
			    <label>Add Image :</label>
			    <input type="file" name="userImage" id="userImage" class="form-control" >   
			</div>

			<div class="form-group">
			    <button type="submit" class="btn btn-success" id="postToFB">Post to Facebook</button> 
			</div>
		</fieldset>
	</form> 



	   <!--  <div class="form-group">
	        <label>Add Image :</label>
	        <input type="file" name="facebook_image" class="form-control">
	    </div> -->
	    
	</div>
	<div class="col-md-2">  </div>
</div> 
<script>
	IMAGE_PATH='';
	$('#facebookPost').keyup(function() {
		if( $('#facebookPost').val() !='') {
			$('#postToFB').attr('disabled',false); 	
		} else {
			$('#postToFB').attr('disabled',true); 	
		}
	});

	$('#FBPOSTTYPE').change(function() {
		if($(this).val()==2) {
			getUserPages($('#UserID').val()); 
			$('#fbPageID').show();
		} else {
			$('#fbPageID').hide();
		}
	});

	function getUserPages(user) {
		$.ajax({
			url:'/posts/user_pages?user='+user,
		    dataType:'json', 
			success:function(r) {
				$('#fbPageIDS').empty();				
				$('#fbPageIDS').append($('<option>').text(r.page.fb_page_name).attr('value', r.page.id)); 
			}
		});
	} 

	$('#UserID').change(function() {
		getUserPages($(this).val());
	});  


	// $('#userImage').change(function(){

	// 	var ext = this.value.match(/\.(.+)$/)[1];
	// 	switch (ext) {
	// 	    case 'jpg':
	// 	    case 'jpeg':
	// 	    case 'png':
	// 	    case 'gif':
	// 	        $('#postToFB').attr('disabled', false);
	// 	        break;
	// 	    default:
	// 	    	alert('Please upload only jpg,jpeg,png,gif images.');
	// 	        $('#postToFB').attr('disabled', true);
	// 	        this.value = '';  
	// 	}

	// 	var iSize = ($("#userImage")[0].files[0].size / 1024);
	// 	iSize = (Math.round((iSize / 1024) * 100) / 100)
	// 	if(iSize>2) {
	// 	  alert('Image should be less than 2 MB');
	// 	  $('#postToFB').attr('disabled', true); 
	// 	} else {
	// 	  $('#postToFB').attr('disabled', false);  
	// 	  $( "#uploadForm" ).submit(); 
	// 	}   
	// }); 
  
	// $(document).ready(function (e) {
	// 	$("#uploadForm").on('submit',(function(e) {
	// 		e.preventDefault();  
	// 		$.ajax({
	//         	url: "/social_media/upload",
	// 			type: "POST",
	// 			data:  new FormData(this),
	// 			beforeSend: function(){$("#body-overlay").show();},
	// 			contentType: false,
	//     	    processData:false,
	// 			success: function(data) {
	// 				data=$.parseJSON(data); 
	// 				IMAGE_PATH=data.completePath;
	// 				setInterval(function() {$("#body-overlay").hide(); },500);
	// 			},
	// 		  	error: function(xhr)  {	
	// 		  		console.log(xhr); 
	// 	    	} 	        
	// 	   });
	// 	}));
	// });

	      

	function postToUserTimeLineWithImage(IMG,access_token,msg) {
		FB.api('/me/photos?access_token='+access_token, 'post', {message : msg, url: IMG, access_token: access_token }, function(response) {
			if (!response || response.error) {
		         alert('Error occured: ' + JSON.stringify(response.error));
            } else {
            	$('#facebookPost').val('');
                alert('Posted successfully to your Facebook Timeline with ID: ' + response.id);
            }
		});
	}
		  
	function postToPage(page, src, msg,token){
	  // var page_id = '314106545771266';
	  FB.api('/'+page+'/photos', 'post', { url: src, message:msg, access_token: token },
	    function(res) { console.log(res) }
	  )
	}
		  
	// $(document).ready(function() {
	// 	$('#postToFB').click(function () {
	// 		if($('#pageIdDiv').is(':visible')) {
	// 			if($('#fbPageID').val()=='') {
	// 				$('#postToFB').attr('disabled',true);
	// 				$('#fbPageID').css({'border-color':'red'});
	// 				$('#fbPageID').focus();
	// 				alert('Please Enter FB Page ID');	
	// 			}
	// 		}
	// 		if( $('#facebookPost').val()=='') {
	// 			$('#postToFB').attr('disabled',true);
	// 			$('#facebookPost').css({'border-color':'red'});
	// 			$('#facebookPost').focus();
	// 			alert('Please Enter Post Text');
	// 		} else {

	// 			window.fbAsyncInit = function() {
	// 				// init the FB JS SDK
	// 				FB.init({
	// 					appId      : '116818605779239',                    // App ID from the app dashboard
	// 					channelUrl : '//local.facebook-test/channel.html',  // Channel file for x-domain comms
	// 					status     : true,                                 // Check Facebook Login status
	// 					xfbml      : true,                                  // Look for social plugins on the page
	// 					oauth      : true                                  // Enable oauth authentication
	// 				});

	// 				// Additional initialization code such as adding Event Listeners goes here
	// 				FB.login(function(response)
	// 				{
	// 					if (response.authResponse)    
	// 					{
	// 						IMAGE_SRC='http://static.dezeen.com/uploads/2013/03/dezeen_Sergio-concept-car-by-Pininfarina_ss_4.jpg';

	// 						if($('#FBPOSTTYPE').val()==2) {

	// 							FB.api('/me/accounts', function(response){ 
	// 								console.log('response');
	// 								console.log(response);
	// 							   for(var i=0;i<=response.data.length;i++) {
	// 							   		if(response.data[i].id==$('#fbPageID').val()) { 
	// 						   				pageAccessToken=response.data[i].access_token;
	// 						   				if(pageAccessToken=='') {  
	// 						   					alert('Cannot post to page because no page access token is received,Please provide correct page id'); 
	// 						   				} else { 
	// 						   					postToPage($('#fbPageID').val(),IMAGE_SRC, $('#facebookPost').val(),pageAccessToken);       	
	// 						   				}    
	// 							   		}  	
	// 							   }  
	// 							});  
	// 						} else {  
	// 							var access_token =   FB.getAuthResponse()['accessToken'];
	// 							postToUserTimeLineWithImage(IMAGE_SRC,access_token,$('#facebookPost').val());
	// 						} 		 
	// 					} else{
	// 						alert('Not logged in');  
	// 					}   
	// 				}, { scope : 'publish_actions,manage_pages,publish_pages' });    

	// 			};

	// 			// Load the SDK asynchronously
	// 			(function(d, s, id){
	// 				var js, fjs = d.getElementsByTagName(s)[0];
	// 				if (d.getElementById(id)) {return;}
	// 				js = d.createElement(s); js.id = id;
	// 				js.src = "//connect.facebook.net/en_US/all.js";
	// 				fjs.parentNode.insertBefore(js, fjs);
	// 			}(document, 'script', 'facebook-jssdk'));
	// 		} 
	// 	});
	// });

</script>
  
@endsection

