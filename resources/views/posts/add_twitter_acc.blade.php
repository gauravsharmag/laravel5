@extends('layouts.back')
@section('title','Add Twitter Account')
@section('content')
	
	<div class="row"> 
		<div class="col-md-2">  </div>  
		<div class="col-md-8">   
			<form method="POST" action="/posts/add_twitter_acc">
			    {{ csrf_field() }}
			    @if(count($errors))   
			        <div class="alert alert-danger">
			            <strong>Whoops!</strong> There were some problems with your input.
			            <br/>
			            <ul> 
			                @foreach($errors->all() as $error)
			                <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>    
			    @endif
			      <span style="color: red;"> Your Twitter App must have <b>Read and Write</b> permissions. </span>
			    <div class="form-group">
			        <label>Tweeter Username:</label>
			       <input type="text" name="twitter_account" class="form-control" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Consumer Key:</label>
			       <input type="text" name="twitter_consumer_key" class="form-control" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Consumer Secret:</label>
			       <input type="text" name="twitter_consumer_secret" class="form-control" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Access Token:</label>
			       <input type="text" name="twitter_access_token" class="form-control" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Access Token Secret:</label>
			       <input type="text" name="twitter_access_token_secret" class="form-control" />
			    </div>

			    <div class="form-group">
			        <button class="btn btn-success">Add Account</button> 
			    </div>
			</form>  
		</div>
		<div class="col-md-2">  </div>
	</div>

@endsection