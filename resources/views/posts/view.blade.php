@extends('layouts.back')
@section('title','View Post')
@section('content') 
	  
	<div class="well well bs-component">
		<div class="content">
			<h2 class="header">{!! $post->title !!}</h2>
			<i class="glyphicon glyphicon-calendar"></i>
			<span> {!! $post->created_at->format('d-m-Y h:i A') !!} </span><br>
			@if($post->scheduled_at)
				<span> <b>Cron Schedule:</b> {!! $post->scheduled_at !!}</span><br>
			@endif

			@if($post->facebook_account_id) 
				<span><b>Facebook Account: </b> {!! $post->facebook_account_name !!}</span><br>
			@endif

			@if($post->twitter_account_id) 
				<span><b>Twitter Account: </b> {!! $post->twitter_account_name !!}</span><br>
			@endif


			<p style="text-align: justify-all;"> {!! $post->content !!} </p> 
			@if(!empty($postImg))
			<div> 
				@foreach($postImg as $p)  
					<img src="/images/social_media/{{$p->image_name}}" style="width:200px;" /> 
				@endforeach
			</div> 
			@endif   
			<br> 
		</div> 
	</div>
	@if(Auth::check())
		@if(Auth::user()->role_id==1)
	    	@if($post->status==0)
				<form action="/posts/approve/{{$post->id}}" method="POST">    
					{{ csrf_field() }}
					<input type="submit" id="approveBTN" class="btn btn-success"  value="Approve" /> 
				</form>  
			@endif
		@endif
	@endif 

<script type="text/javascript">
	$('#approveBTN').click(function(e) {  
		if(window.confirm('Are you sure?')) {
			return true;
		} else {
			e.preventDefault();
			return false;
		} 	
	});
</script>

@endsection