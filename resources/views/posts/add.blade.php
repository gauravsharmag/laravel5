@extends('layouts.back')
@section('title','Add Post To Twitter')  
@section('content')  
<div class="row"> 
	<div class="col-md-2">  </div>  
	<div class="col-md-8">  
		
		<form method="POST" action="{{ route('post.tweet') }}" enctype="multipart/form-data">

		    {{ csrf_field() }}

		    @if(count($errors))
		        <div class="alert alert-danger">
		            <strong>Whoops!</strong> There were some problems with your input.
		            <br/>
		            <ul>
		                @foreach($errors->all() as $error)
		                <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif
		    <div class="form-group"> 
		    	<label>Select Twitter Account:</label>
		    	{!! Form::select('twitter_account', $accounts, null,['class'=>'form-control'] ) !!} 
		    	
		    </div>
		    <div class="form-group">
		        <label>Add Tweet Text:</label>
		        <textarea class="form-control" name="tweet" ></textarea> 
		    </div>
		    <div class="form-group">
		        <label>Add Multiple Images (upto 4):</label>
		        <input type="file" name="images[]" id="twitter_images" multiple class="form-control">
		    </div>
		    <div class="form-group">
		        <button class="btn btn-success" id="btn_tweet">Add New Tweet</button>
		    </div>
		</form>  
	</div>
	<div class="col-md-2">  </div>
</div> 

<script type="text/javascript">
	
	$('#twitter_images').change(function () {
	    var ext = this.value.match(/\.(.+)$/)[1];
	    switch (ext) {
	        case 'jpg':
	        case 'jpeg':
	        case 'png':
	        case 'gif':  
	            $('#btn_tweet').attr('disabled', false);
	            break; 
	        default:
	        	alert('Please upload only jpg,jpeg,png,gif images.');
	            $('#btn_tweet').attr('disabled', true);
	            this.value = '';  
	    }
	    var iSize = ($("#twitter_images")[0].files[0].size / 1024);
	    iSize = (Math.round((iSize / 1024) * 100) / 100)
	    if(iSize>2) { 
	      alert('Image should be less than 2 MB');
	      $('#btn_tweet').attr('disabled', true); 
	    } else {
	      $('#btn_tweet').attr('disabled', false);  
	    }     
	});

</script>


@endsection