@extends('layouts.back')
@section('title','Edit Post')  
@section('content') 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> 
<div class="row"> 
	<div class="col-md-2">  </div>  
	<div class="col-md-8">  
		
		<form method="POST" action="/posts/edit/{{$post['id']}}" enctype="multipart/form-data">

		    {{ csrf_field() }} 

		    @if(count($errors))
		        <div class="alert alert-danger">
		            <strong>Whoops!</strong> There were some problems with your input.
		            <br/>
		            <ul>
		                @foreach($errors->all() as $error)
		                <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif 
		     @if($post['facebook_account_id']) 
			    <div class="form-group" id="fb_user">
			        <label>Select Facebook User:</label>   
			        {!! Form::select('fb_user',[''=>'Select FB User']+$fbUsers->toArray(),$post['facebook_account_id'] ,['class'=>'form-control','id'=>'fbUserID'] ) !!}  
			    </div>
		    
			    <div class="form-group">
			        <label>Select Post Type:</label>   
			        {!! Form::select('fb_post_type',[1=>"Post To FB User Timeline",2=>"Post To FB Page"],$post['fb_post_type'] ,['class'=>'form-control','id'=>'fbUserID'] ) !!}     
			    </div>
			@endif
		    @if($post['twitter_account_id'])
			    <div class="form-group" id="tw_user"> 
			        <label>Select Twitter User:</label>   
			        {!! Form::select('tw_user',[''=>'Select Twitter User']+$twUsers->toArray(), $post['twitter_account_id'],['class'=>'form-control','id'=>'twUserID'] ) !!}       
			    </div>
		    @endif

		    <div class="form-group">
		        <label>Title:</label>
		        <input type="text" name="post_title" id="postTitle" class="form-control" value="{{$post['title']}}">
		    </div>
		    <div class="form-group">
		        <label>Content:</label>
		        <textarea name="post_content" id="postContent" class="form-control">
		        	{{$post['content']}}
		        </textarea> 
		    </div>
		    <div class="form-group">
		        <label>Image:</label> 
		        <div>  
		        	<!-- <img src="/images/social_media/{{$post['image']}}"  width="200px;"> -->
		        </div>
		        <input type="file" name="post_images[]" id="postImage" multiple="true" class="form-control"> 
		    </div>
		    <div class="form-group">
		        <label>Schedule:</label> 
		        <input type="text" name="scheduled_at" id="scheduled_at"  class="form-control" value="{{$post['scheduled_at']}}"> 
		    </div> 
		    @if($post['post_images'])
			    <div id="currentImages"> 
			    	@foreach($post['post_images'] as $pI)
			    		<a href="javascript:;" id={{$pI['id']}}"" onclick="deleteImg({{$pI['id']}})">X
			    			<img src="/images/social_media/{{$pI['image_name']}}"  width="200px;">	
			    		</a>
			    	@endforeach	
			    </div>
		    @endif
		    <div class="form-group">
		        <button class="btn btn-success" id="post_tbn">Add Post</button>
		    </div>
		</form>  
	</div>
	<div class="col-md-2">  </div>
</div> 
  
<script type="text/javascript">
	$('#scheduled_at').datetimepicker({  
		format: 'DD-MM-YYYY HH:mm',
		stepping: 5,
		showClose:true,
		showClear:true 
	});       
	$('#postType').change(function() {
		if($(this).val()==1) {
			$('#fb_user').show();
			$('#fbPostType').show();
			$('#tw_user').hide();
		} else if($(this).val()==2) {
			$('#fb_user').hide();
			$('#fbPostType').hide();
			$('#tw_user').show();
		} else if($(this).val()==3) {
			$('#fb_user').show();
			$('#fbPostType').show();
			$('#tw_user').show();
		} else {
			$('#fb_user').hide();
			$('#fbPostType').hide();
			$('#tw_user').hide();
		}
	});

	function deleteImg(id) { 
		if(id) {
			$.ajax({
				url:'/posts/deleteImages/'+id,
				dataType:'json',
				success:function(r) {
					console.log(r);
					if(r.success) {
						alert('Post Image Deleted Succssfully.');
						window.location.reload();
					}

				}
			});
		}
	}

	// $('#twitter_images').change(function () {
	//     var ext = this.value.match(/\.(.+)$/)[1];
	//     switch (ext) {
	//         case 'jpg':
	//         case 'jpeg':
	//         case 'png':
	//         case 'gif':  
	//             $('#btn_tweet').attr('disabled', false);
	//             break; 
	//         default:
	//         	alert('Please upload only jpg,jpeg,png,gif images.');
	//             $('#btn_tweet').attr('disabled', true);
	//             this.value = '';  
	//     }
	//     var iSize = ($("#twitter_images")[0].files[0].size / 1024);
	//     iSize = (Math.round((iSize / 1024) * 100) / 100)
	//     if(iSize>2) { 
	//       alert('Image should be less than 2 MB');
	//       $('#btn_tweet').attr('disabled', true); 
	//     } else {
	//       $('#btn_tweet').attr('disabled', false);  
	//     }     
	// });

</script>


@endsection