@extends('layouts.back')
@section('title','Add New Post')  
@section('content')  
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<div class="row"> 
	<div class="col-md-2">  </div>  
	<div class="col-md-8">  
		<form method="POST" action="/posts/add_post" enctype="multipart/form-data">
		    {{ csrf_field() }}

		    @if(count($errors))  
		        <div class="alert alert-danger">  
		            <strong>Whoops!</strong> There were some problems with your input.
		            <br/>
		            <ul>
		                @foreach($errors->all() as $error) 
		                <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif
		    <div class="form-group" id="postTypeDiv">
		        <label>Select Post Type:</label>   
		        <select name="post_type" id="postType" class="form-control">
		        	<option value="">Select</option>
		        	<option value="1">Facebook</option>
		        	<option value="2">Twitter</option>
		        	<option value="3">Both</option> 
		        </select>
		    </div>
		    <div class="form-group" id="fb_user" style="display: none;">
		        <label>Select Facebook User:</label>   
		        {!! Form::select('fb_user',[''=>'Select FB User']+$fbUsers->toArray(), null,['class'=>'form-control','id'=>'fbUserID'] ) !!}  
		    </div>

		    <div class="form-group" id="fbPostType" style="display: none;">
		        <label>Select FB Post Type:</label>   
		        <select name="fb_post_type" id="FBPOSTTYPE" class="form-control">
		        	<option value="1">Post To FB User Timeline</option>
		        	<option value="2">Post To FB Page</option>   
		        </select>
		    </div>

		    <div class="form-group" id="tw_user" style="display: none;"> 
		        <label>Select Twitter User:</label>   
		        {!! Form::select('tw_user',[''=>'Select Twitter User']+$twUsers->toArray(), null,['class'=>'form-control','id'=>'twUserID'] ) !!}        
		    </div>
		    
		    <div class="form-group">
		        <label>Title (This will be posted to social media):</label>
		        <input type="text" name="post_title" id="postTitle" class="form-control">
		    </div>
		    <div class="form-group">
		        <label>Content:</label>
		        <textarea name="post_content" id="postContent" class="form-control"></textarea> 
		    </div>
		    <div class="form-group">
		        <label>Image:</label>
		        <input type="file" name="post_images[]" id="postImage" multiple="true"  class="form-control"> 
		    </div>
		    <div class="form-group">
		        <label>Schedule:</label> 
		        <input type="text" name="scheduled_at" id="scheduled_at"  class="form-control"> 
		    </div>
 
		    <div class="form-group">
		        <button class="btn btn-success" id="post_tbn">Add Post</button>
		    </div>
		</form>    
	</div>  
	<div class="col-md-2">  </div>
</div> 
<script type="text/javascript">
	$('#scheduled_at').datetimepicker({  
		format: 'DD-MM-YYYY HH:mm',
		stepping: 5,
		showClose:true,
		showClear:true 
	}); 
	$('#postType').change(function() {
		if($(this).val()==1) {
			$('#fb_user').show();
			$('#fbPostType').show();
			$('#tw_user').hide();
		} else if($(this).val()==2) {
			$('#fb_user').hide();
			$('#fbPostType').hide();
			$('#tw_user').show();
		} else if($(this).val()==3) {
			$('#fb_user').show();
			$('#fbPostType').show();
			$('#tw_user').show();
		} else {
			$('#fb_user').hide();
			$('#fbPostType').hide();
			$('#tw_user').hide();
		}
	});
</script>
@endsection