@extends('layouts.back')
@section('title','View Twitter Account')
@section('content') 
	  
	<div class="well well bs-component">  
		<div class="content">
			<h2 class="header">{!! $tw->twitter_account !!}</h2>
			<i class="glyphicon glyphicon-calendar"></i>
			<span> {!! $tw->created_at !!} </span> <br>
			<p><b>Twitter Consumer Key:</b> {!! $tw->twitter_consumer_key !!} </p>
			<p><b>Twitter Consumer Secret:</b> {!! $tw->twitter_consumer_secret !!} </p>
			<p><b>Twitter Access Token:</b> {!! $tw->twitter_access_token !!} </p>
			<p><b>Twitter Access Token Secret:</b> {!! $tw->twitter_access_token_secret !!} </p> 
		</div> 
	</div> 
  

@endsection