@extends('layouts.back')
@section('title','View All Posts')
@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>	

	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> Posts </h2>
		</div> 
		<table class="table" id="postsTable">
			<thead>
				<tr>
					<!-- <th>ID</th> -->
					<th>Title</th>
					<th>Created On</th>
					<th>Status</th>
					<th>Posted On FB</th>
					<th>Posted On Twitter</th>
					<th>Action</th> 
				</tr>
			</thead>
		</table>	
	</div>  


<script>
	$(function() {     
	    $('#postsTable').DataTable({ 
	        processing: true,
	        serverSide: true,  
	        ajax: "{{ route('datatable.getposts') }}",    
	        columns: [
	            // { data: 'id', name: 'id' },
	            { data: 'title', name: 'title' },
	            { data: 'created_at', name: 'created_at' }, 
	            { data: 'status', name: 'status' }, 
	            { data: 'is_posted_on_fb', name: 'is_posted_on_fb' },
	            { data: 'is_posted_on_tw', name: 'is_posted_on_tw' }, 
	            { data: 'action', name: 'action'}    
	        ]
	    });
	});


	function confirmDel(id) {
		if(window.confirm('Are you sure?')) {
			window.location.href='/posts/delete/'+id; 
		} else { 
			return false;
		} 
	}
</script>
@endsection

