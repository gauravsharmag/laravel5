@extends('layouts.back')
@section('title','Update Facebook Account')
@section('content')  
	
	<div class="row"> 
		<div class="col-md-2">  </div>  
		<div class="col-md-8">   
			<form method="POST" action="/posts/edit_fb_acc/{{$fb->id}}">
			    {{ csrf_field() }}
			    @if(count($errors))   
			        <div class="alert alert-danger">
			            <strong>Whoops!</strong> There were some problems with your input.
			            <br/>
			            <ul>     
			                @foreach($errors->all() as $error)
			                <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>
			    @endif
			    <span style="color: red;"> Your Facebook App must have <b>publish_actions,manage_pages,publish_pages</b> permissions. </span>
			    <div class="form-group">
			        <label>Facebook UserName:</label>
			       <input type="text" name="fb_user_name" class="form-control" value="{{$fb->fb_user_name}}" />
			    </div>
			    <div class="form-group">  
			        <label>Facebook App ID:</label>
			       <input type="text" name="fb_app_id" class="form-control" value="{{$fb->fb_app_id}}" />
			    </div>
			    <div class="form-group">
			        <label>Facebook App Secret:</label>
			       <input type="text" name="fb_app_secret" class="form-control" value="{{$fb->fb_app_secret}}"/> 
			    </div>
			    <div class="form-group">
			        <label>Facebook User Access Token:</label>
			       <input type="text" name="fb_user_access_token" class="form-control" value="{{$fb->fb_user_access_token}}"/>
			    </div>
			    <div class="form-group">
			        <label>Facebook Page ID:</label>
			       <input type="text" name="fb_page_id" class="form-control"  value="{{$fb->fb_page_id}}"/>
			    </div>
			    <div class="form-group">
			        <label>Facebook Page Name:</label>
			       <input type="text" name="fb_page_name" class="form-control" value="{{$fb->fb_page_name}}"/>  
			    </div>
			       

			    <div class="form-group">
			        <button class="btn btn-success">Add Account</button> 
			    </div>
			</form>  
		</div>
		<div class="col-md-2">  </div>
	</div>

@endsection