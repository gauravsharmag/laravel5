@extends('layouts.back')
@section('title','View Facebook Account')
@section('content') 
	  
	<div class="well well bs-component">  
		<div class="content">
			<h2 class="header">{!! $fb->fb_user_name !!}</h2>
			<i class="glyphicon glyphicon-calendar"></i>
			<span> {!! $fb->created_at !!} </span> <br>
			<p><b>Fb App Id:</b> {!! $fb->fb_app_id !!} </p>
			<p><b>Fb App Secret:</b> {!! $fb->fb_app_secret !!} </p>
			<p style="text-align: justify-all;"><b>Fb User Access Token:</b> {!! $fb->fb_user_access_token !!} </p>
			<p><b>Fb Page Id:</b> {!! $fb->fb_page_id !!} </p>
			<p><b>Fb Page Name:</b> {!! $fb->fb_page_name !!} </p>
		</div> 
	</div> 
  

@endsection