@extends('layouts.back')
@section('title','All Twitter Accounts')
@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>	

	<div class="panel panel-default">
		<div class="panel-heading">
			<h2> Twitter Accounts </h2>
		</div> 
		<table class="table" id="postsTable">
			<thead>
				<tr>   
					<th>Account</th>
					<th>Created On</th>
					<th>Consumer Key</th>
					<th>Action</th>     
				</tr>
			</thead>
		</table>	
	</div>  


<script>
	$(function() {     
	    $('#postsTable').DataTable({ 
	        processing: true,
	        serverSide: true,  
	        ajax: "{{ route('datatable.gettwitter') }}",    
	        columns: [
	            { data: 'twitter_account', name: 'twitter_account' },
	            { data: 'created_at', name: 'created_at' }, 
	            { data: 'twitter_consumer_key', name: 'twitter_consumer_key' },
	            { data: 'action', name: 'action'}     
	        ]
	    }); 
	});

	$(document).ready(function() {
		// $('#postDeleteBTN').click(function(e) {
		// 	alert('called');
		// 	console.log($(this)); 
			// if(window.confirm('Are you sure?')) {
			// 	window.location.href='/posts/delete/'+id; 
			// } else {
			// 	e.preventDefault();
			// 	return false;
			// } 
		// });
	});

	function confirmDel(id) {
		if(window.confirm('Are you sure?')) {
			window.location.href='/posts/delete/'+id; 
		} else {
			return false;
		} 
	}
</script>
@endsection

