@extends('layouts.back')
@section('title','Edit Twitter Account')
@section('content')
	
	<div class="row"> 
		<div class="col-md-2">  </div>    
		<div class="col-md-8">   
			<form method="POST" action="/posts/edit_twitter_acc/{{$twitter->id}}">
			    {{ csrf_field() }}
			    @if(count($errors))   
			        <div class="alert alert-danger">
			            <strong>Whoops!</strong> There were some problems with your input.
			            <br/>
			            <ul> 
			                @foreach($errors->all() as $error)
			                <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>
			    @endif
			    <div class="form-group">
			        <label>Tweeter Username:</label>
			       <input type="text" name="twitter_account" class="form-control" value="{{$twitter->twitter_account}}" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Consumer Key:</label>
			       <input type="text" name="twitter_consumer_key" class="form-control" value="{{$twitter->twitter_consumer_key}}" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Consumer Secret:</label>
			       <input type="text" name="twitter_consumer_secret" class="form-control" value="{{$twitter->twitter_consumer_secret}}" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Access Token:</label>
			       <input type="text" name="twitter_access_token" class="form-control" value="{{$twitter->twitter_access_token}}" />
			    </div>
			    <div class="form-group">
			        <label>Tweeter Access Token Secret:</label>
			       <input type="text" name="twitter_access_token_secret" class="form-control"  value="{{$twitter->twitter_access_token_secret}}"/> 
			    </div>

			    <div class="form-group">
			        <button class="btn btn-success">Add Account</button> 
			    </div>
			</form>  
		</div>
		<div class="col-md-2">  </div>
	</div>

@endsection